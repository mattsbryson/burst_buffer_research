/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*										*/
/*   Files:         Have to list the files  					*/
/*										*/	
/*   Description:  BDATS: BigData Analytics at Trillion Scale    		*/ 
/*                                                                           	*/
/*   Author:  Md. Mostofa Ali Patwary                                        	*/
/*            Research Scientist, Parallel Computing Lab, Intel Corporation    	*/
/*            email: mostofa.ali.patwary@intel.com                          	*/
/*										*/
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

#ifndef _BDATS_HEADERS_
#define _BDATS_HEADERS_

#include <mpi.h>
#include <unistd.h>
#include <omp.h>
#include <algorithm>
#include <vector>
#include <map>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <climits>
#include <string>
#include <cstring>
#include <float.h>
#include <queue>
#include <assert.h> 
using namespace std;

#endif
