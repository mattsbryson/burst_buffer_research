/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                              */
/*   Files:         Have to list the files                                      */
/*                                                                              */
/*   Description:  HDF5 Reader for BDATS: BigData Analytics at Trillion Scale   */
/*                                                                              */
/*   Author:  Suren Byna		                                        */
/*            Research Scientist, Lawrence Berkeley National Lab    		*/
/*            email: SByna@lbl.gov                              		*/
/*                                                                              */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

#include "bdats_h5reader.h"

void  open_datasets (dataset_info_t *data_info_array, Points *pts)
{
        hid_t   dset_id;
        hsize_t proc_offset;
        int     space_ndims;
        hid_t  dataspace;
        dset_id  = H5Dopen(data_info_array->open_file_id, data_info_array->name, H5P_DEFAULT);
	if (dset_id < 0)
	{
		printf ("Error opening dataset: %s, in open_datasets [H5Dopen] \n", data_info_array->name);
		exit(1);
	}
	
        dataspace = H5Dget_space(dset_id);
	if (dataspace < 0)
	{
		printf ("Error retrieving dataspace for dataset: %s, in open_datasets [H5Dget_space] \n", data_info_array->name);
		exit(1);
	}
	
        space_ndims = H5Sget_simple_extent_ndims(dataspace);
	if (space_ndims < 0)
	{
		printf ("Error retrieving ndims for dataset: %s, in open_datasets [H5Sget_simple_extent_ndims] \n", data_info_array->name);
		exit(1);
	}
	

        proc_offset = data_info_array->mpi_rank * (pts->m_num_global_points/data_info_array->mpi_size);

        data_info_array->open_dataset_id = dset_id;
        data_info_array->local_offset    = proc_offset;
        data_info_array->global_size     = pts->m_num_global_points;
        data_info_array->local_size      = pts->m_num_local_points;
        data_info_array->open_space_id   = dataspace;
        data_info_array->space_ndims     = space_ndims;
}

void read_segment(dataset_info_t * cur_dataset)
{
        hid_t plist2_id, memspace;
	int retval;

        memspace =  H5Screate_simple(cur_dataset->space_ndims, &(cur_dataset->local_size), NULL);
	if (memspace < 0)
	{
		printf ("Error creating dataspace for dataset: %s, in read_segment [H5Screate_simple] \n", cur_dataset->name);
		exit(1);
	}

        plist2_id = H5Pcreate(H5P_DATASET_XFER);
	if (plist2_id < 0)
	{
		printf ("Error in creating properties for dataset: %s, in read_segment [H5Pcreate] \n", cur_dataset->name);
		exit(1);
	}
	
        retval = H5Pset_dxpl_mpio(plist2_id, H5FD_MPIO_COLLECTIVE);
	if (retval < 0)
	{
		printf ("Error in setting properties for dataset: %s, in read_segment [H5Pset_dxpl_mpio] \n", cur_dataset->name);
		exit(1);
	}
	

        hsize_t my_offset, my_size;
        my_offset = cur_dataset->local_offset;
        my_size   = cur_dataset->local_size;
        retval = H5Sselect_hyperslab(cur_dataset->open_space_id, H5S_SELECT_SET, &my_offset, NULL, &my_size , NULL);
	if (retval < 0)
	{
		printf ("Error in selecting hyperslab for dataset: %s, in read_segment [H5Sselect_hyperslab] \n", cur_dataset->name);
		exit(1);
	}
	

        retval = H5Dread(cur_dataset->open_dataset_id, H5T_NATIVE_FLOAT, memspace, cur_dataset->open_space_id, plist2_id, cur_dataset->buf);
	if (retval < 0)
	{
		printf ("Error in reading dataset: %s, in read_segment [H5Dread] \n", cur_dataset->name);
		exit(1);
	}
	
        retval = H5Pclose(plist2_id);
	if (retval < 0)
	{
		printf ("Error in closing properties for dataset: %s, in read_segment [H5Pclose] \n", cur_dataset->name);
		exit(1);
	}
	
}

void get_metadata_all_datasets (char* filename, dataset_info_t *data_info_array, Points *pts, int mpi_rank, int mpi_size)
{
        int dataset_count;
        H5PartFile* H5P_fileid;
        H5P_fileid = H5PartOpenFileParallel(filename, H5PART_READ, MPI_COMM_WORLD);
        dataset_count = H5PartGetNumDatasets (H5P_fileid);

        int num_timesteps = H5PartGetNumSteps (H5P_fileid);

        // char step_name[OBJ_NAME_MAX];
        // if (H5PartReadStepAttrib(H5P_fileid, "filename", &step_name[0]) == 1)
        // {
                // printf("Read step from file: %s\n", step_name);
        // }

        // h5part_int64_t num_attribs = H5PartGetNumStepAttribs(H5P_fileid);
        // fprintf(stdout, "Number of step attributes in step #%lld: %lld\n", num_timesteps, (long long)num_attribs);

        int index;
        char   dataset_name[OBJ_NAME_MAX];

        pts->m_num_global_points = H5PartGetNumParticles (H5P_fileid);
        pts->m_num_dims = dataset_count;
        for(index=0; index < dataset_count; index++)
        {
                data_info_array[index].mpi_rank = mpi_rank;
                data_info_array[index].mpi_size = mpi_size;
                H5PartGetDatasetName(H5P_fileid, index, dataset_name, OBJ_NAME_MAX);
                // Replace the hard-coded time step name
                sprintf (data_info_array[index].name, "/Step#0/%s", dataset_name);
                // printf("\tDataset[%u] name= %s\n", index, data_info_array[index].name);
        }

	if (mpi_rank == 0) {
	    cout << "pts->m_num_global_points " << pts->m_num_global_points << " pts->m_num_dims " << pts->m_num_dims << endl;
	}

        H5PartCloseFile(H5P_fileid);
}

void get_metadata_select_datasets (char* filename, dataset_info_t *data_info_array, Points *pts, int mpi_rank, int mpi_size, int dataset_count)
{
        H5PartFile* H5P_fileid;
        H5P_fileid = H5PartOpenFileParallel(filename, H5PART_READ, MPI_COMM_WORLD);

        int num_timesteps = H5PartGetNumSteps (H5P_fileid);

        int index;
        char   dataset_name[OBJ_NAME_MAX];

        pts->m_num_global_points = H5PartGetNumParticles (H5P_fileid);
        pts->m_num_dims = dataset_count;
        for(index=0; index < dataset_count; index++)
        {
                data_info_array[index].mpi_rank = mpi_rank;
                data_info_array[index].mpi_size = mpi_size;
                // printf("\tDataset[%u] name= %s\n", index, data_info_array[index].name);
        }

	if (mpi_rank == 0) {
	    cout << "pts->m_num_global_points " << pts->m_num_global_points << " pts->m_num_dims " << pts->m_num_dims << endl;
	}

        H5PartCloseFile(H5P_fileid);
}

void allocate_memory (Points *pts)
{
        BDATS_decimal** data_array;
        int dim;
        data_array = (BDATS_decimal**) malloc (pts->m_num_dims * sizeof (BDATS_decimal*));
        for (dim = 0; dim < pts->m_num_dims; dim++) {
                data_array[dim] = (BDATS_decimal*) malloc (pts->m_num_local_points * sizeof (BDATS_decimal));
                if(data_array[dim] == NULL)
                {
                        printf("Memory allocation for data_array[%d] failed... \n", dim);
                        exit(-1);
                }
        }

        pts->m_data = data_array;
}

void read_driver (char *filename, Points *pts, MPI_Comm comm, MPI_Info info, int mpi_rank, int mpi_size, dataset_info_t *data_info_array)
{
        dataset_info_t *current_dataset;
        hid_t  file_id, plist_id;
        hsize_t         local_size;

        plist_id = H5Pcreate(H5P_FILE_ACCESS);
        H5Pset_fapl_mpio(plist_id, comm, info);
        file_id = H5Fopen(filename, H5F_ACC_RDONLY,plist_id);

        double t1, t2;
        MPI_Barrier(MPI_COMM_WORLD);
        t1 =  MPI_Wtime();

        int i, index;
        int64_t last_process_extra_pts = 0;

        // Assuming that each dataset has the same size
        last_process_extra_pts = pts->m_num_global_points % mpi_size;
        if (mpi_rank ==  (mpi_size - 1))
        {
                pts->m_num_local_points = pts->m_num_global_points/mpi_size + last_process_extra_pts;
        }
        else
        {
                pts->m_num_local_points = pts->m_num_global_points/mpi_size;
        }

        // Allocate memory for Indexes
        pts->m_vec_global_IDs =  (BDATS_gIndex*) malloc (pts->m_num_local_points * sizeof(BDATS_gIndex));
        if(pts->m_vec_global_IDs == NULL)
        {
                printf("Memory allocation for m_vec_global_IDs failed... \n");
                exit(-1);
        }

        // int64_t global_offset = pts->m_num_local_points*mpi_rank;
        int64_t global_offset = pts->m_num_global_points/mpi_size;
        for (index = 0; index < pts->m_num_local_points; index++)
        {
                pts->m_vec_global_IDs[index] = global_offset + index;
        }

        // Allocate memory for data
        allocate_memory (pts);

        // Go through each HDF5 dataset and read data into certain locations.
        for(i = 0; i < pts->m_num_dims; i++)
        {
                current_dataset = &(data_info_array[i]);
                current_dataset->open_file_id = file_id;

                //Open datasets and set hyperslab boundaries
                open_datasets (current_dataset, pts);

                current_dataset->buf = *&pts->m_data[i];
                if(current_dataset->buf == NULL)
                {
                        printf("Memory allocation for reading data fails ! \n");
                        exit(-1);
                }

                //Parallel read
		// printf ("i: %d, num_local_points: %lld, rank: %d \n", i, pts->m_num_local_points, mpi_rank);
                read_segment(current_dataset);
                if (mpi_rank == 0)
                        printf ("Finished reading dataset[%d] \n", i);
        }

        H5Pclose(plist_id);
        MPI_Barrier(MPI_COMM_WORLD);
        t2 =  MPI_Wtime();

	// Calculate field density for Vadim
	count_particles(0.3, pts->m_num_local_points, pts, comm, mpi_rank);

	#ifdef _BDATS_DEBUG_
        if(mpi_rank == 0 ){
                printf("Data read time %f \n", t2 - t1);
        }
	#endif

        #ifdef DEBUG
                // Print the first 100 elements in each dataset
                int64_t j;
                for(i = 0 ; i < pts->m_num_dims; i++)
                {
                        printf ("\nDataset [%d]: \n", i);
                        for(j = 0; j < 100; j++)
                        {
                                printf ("%f \t", pts->m_data[i][j]);
                        }
                }
                // Print the first 100 global indexes
                printf ("\nGlobal indexes of rank [%d]: ", mpi_rank);
                for (index = 0; index < 100; index++)
                {
                        printf ("%lld \t", pts->m_vec_global_IDs[index]);
                }
                printf ("\n");
        #endif

        MPI_Barrier(MPI_COMM_WORLD);

        for(i = 0; i < pts->m_num_dims; i++)
        {
                current_dataset = &(data_info_array[i]);
                H5Sclose(current_dataset->open_space_id);
                H5Dclose(current_dataset->open_dataset_id);
        }

        H5Fclose(file_id);
        //free (pts->m_data);
        //free (pts->m_vec_global_IDs);
}


int read_hdf5_file(char* filename, Points *pts, dataset_info_t*  data_info_array, int dataset_count)
{
	int    mpi_size, mpi_rank;

     	MPI_Comm comm = MPI_COMM_WORLD;
        MPI_Info info = MPI_INFO_NULL;

        MPI_Comm_size(comm, &mpi_size);
        MPI_Comm_rank(comm, &mpi_rank);

	if(dataset_count == 0) // read all columns
	        get_metadata_all_datasets (filename, data_info_array, pts, mpi_rank, mpi_size);
	else
	        get_metadata_select_datasets (filename, data_info_array, pts, mpi_rank, mpi_size, dataset_count);

        // Read data
        read_driver (filename, pts, comm, info, mpi_rank, mpi_size, data_info_array);

	return 0;
}	 

H5PartFile* init_h5outfile (char* filename)
{
	H5PartFile* H5_out_fileid;
	H5_out_fileid = H5PartOpenFileParallel (filename, H5PART_WRITE | H5PART_FS_LUSTRE, MPI_COMM_WORLD);

	if (H5_out_fileid == NULL)
	{
		printf ("Error in opening file for init_h5outfile dataset \n");
		exit(-1);
	}
	return (H5_out_fileid);
}

int write_dataset_BDATS_lIndex (char* dataset_name, BDATS_lIndex* ids, int64_t num_pts, H5PartFile *H5_out_fileid)
{
	int    mpi_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
	int retval;

	double t1, t2;
        MPI_Barrier(MPI_COMM_WORLD);
        t1 =  MPI_Wtime();

	retval = H5PartSetStep(H5_out_fileid, 0);  // Assuming only one time step, with name /Step#0/
	if (retval != H5PART_SUCCESS)
	{
		printf ("Error in H5PartSetStep in init_h5outfile \n");
		exit(-1);
	}

	retval = H5PartSetNumParticles(H5_out_fileid, num_pts);
	if (retval != H5PART_SUCCESS)
	{
		printf ("[Rank: %d] Error in H5PartSetNumParticles in writing dataset: %s \n", mpi_rank, dataset_name);
	}

	retval = H5PartWriteDataInt32(H5_out_fileid, dataset_name, ids);
	if (retval != H5PART_SUCCESS)
	{
		printf ("[Rank: %d] Error in H5PartWriteDataInt32 in writing dataset: %s \n", mpi_rank, dataset_name);
	}

        MPI_Barrier(MPI_COMM_WORLD);
	t2 =  MPI_Wtime();

	#ifdef _BDATS_DEBUG_	
        if(mpi_rank == 0 ){
                printf("Time to write dataset %s: %f \n", dataset_name, t2 - t1);
        }
	#endif	
}

int write_dataset_BDATS_gIndex (char* dataset_name, BDATS_gIndex* ids, int64_t num_pts, H5PartFile *H5_out_fileid)
{
	int    mpi_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
	int retval;

	double t1, t2;
        MPI_Barrier(MPI_COMM_WORLD);
        t1 =  MPI_Wtime();

	retval = H5PartSetStep(H5_out_fileid, 0);  // Assuming only one time step, with name /Step#0/
	if (retval != H5PART_SUCCESS)
	{
		printf ("Error in H5PartSetStep in init_h5outfile \n");
		exit(-1);
	}

	retval = H5PartSetNumParticles(H5_out_fileid, num_pts);
	if (retval != H5PART_SUCCESS)
	{
		printf ("[Rank: %d] Error in H5PartSetNumParticles in writing dataset: %s \n", mpi_rank, dataset_name);
	}

	retval = H5PartWriteDataInt64(H5_out_fileid, dataset_name, ids);
	if (retval != H5PART_SUCCESS)
	{
		printf ("[Rank: %d] Error in H5PartWriteDataInt64 in writing dataset: %s \n", mpi_rank, dataset_name);
	}

        MPI_Barrier(MPI_COMM_WORLD);
	t2 =  MPI_Wtime();

	#ifdef _BDATS_DEBUG_	
        if(mpi_rank == 0 ){
                printf("Time to write dataset %s: %f \n", dataset_name, t2 - t1);
        }
	#endif	
}

int write_dataset_BDATS_decimal (char* dataset_name, BDATS_decimal* data, int64_t num_pts, H5PartFile *H5_out_fileid)
{
	int    mpi_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
	int retval;

	retval = H5PartSetStep(H5_out_fileid, 0);  // Assuming only one time step, with name /Step#0/
	if (retval != H5PART_SUCCESS)
	{
		printf ("[Rank: %d] Error in H5PartSetStep in writing dataset: %s \n", mpi_rank, dataset_name);
	}

	retval = H5PartSetNumParticles(H5_out_fileid, num_pts);
	if (retval != H5PART_SUCCESS)
	{
		printf ("[Rank: %d] Error in H5PartSetNumParticles in writing dataset: %s \n", mpi_rank, dataset_name);
	}

	double t1, t2;
        MPI_Barrier(MPI_COMM_WORLD);
        t1 =  MPI_Wtime();

	retval = H5PartWriteDataFloat32(H5_out_fileid, dataset_name, data);
	if (retval != H5PART_SUCCESS)
	{
		printf ("[Rank: %d] Error in H5PartWriteDataFloat32 in writing dataset: %s \n", mpi_rank, dataset_name);
	}

        MPI_Barrier(MPI_COMM_WORLD);
	t2 =  MPI_Wtime();
	#ifdef _BDATS_DEBUG_
        if(mpi_rank == 0 ){
                printf("Time to write dataset %s: %f \n", dataset_name, t2 - t1);
        }
	#endif
}

int close_h5outfile (H5PartFile *H5_out_fileid)
{
	int retval = H5PartCloseFile (H5_out_fileid);
	if (retval != H5PART_SUCCESS)
	{
		printf ("Error in H5PartCloseFile in close_h5outfile \n");
		exit (-1);
	}
}


/*
 Vadim's code for grid density counting... 
 bin the particles into a 3D grid; count the number of particles in a 2D slice
 of this grid. the result is saved as a gda file (block binary)
 Parameters: 
    eps : size of the cell
    MPI_comm & mpi_rank : MPI communicator and rank   
*/
void count_particles(double eps, int64_t size_of_local_dataset, Points *pts, MPI_Comm comm, int mpi_rank) 
{
  struct grid_t grid;                     // local grid
  struct grid_t global_grid;	          // global grid
  int64_t i,N, ii,jj,kk;                      

  // parameters of the simulation box 
  grid.x0 = 0;
  grid.x1 = 330.0;
  grid.y0 = -0.5*330;
  grid.y1 =  0.5*330;
  grid.z0 = -0.5*132.0;
  grid.z1 =  0.5*132.0;
   
  // initialize grid: resolution

  grid.nx = (int) ((grid.x1-grid.x0)/eps);
  grid.nz = (int) ((grid.z1-grid.z0)/eps);
  

  grid.dx = (grid.x1-grid.x0)/grid.nx;
  grid.dy = eps;
  grid.dz = (grid.z1-grid.z0)/grid.nz;
  
  if (mpi_rank == 0) {
    printf("-------------------------------\n");
    printf("nx=%i, nz=%i\n",grid.nx,grid.nz);
    printf("dx=%i, dz=%i\n",grid.dx,grid.dz);
    printf("-------------------------------\n");
  }

  // allocate density array
  grid.density = (float *) malloc(sizeof(float)*grid.nx*grid.nz);  
  for (i=0; i<grid.nx*grid.nz; i++) 
	grid.density[i] = 0;

  // initialize global grid on rank = 0
  if (mpi_rank == 0) 
  {  
    global_grid = grid;
    global_grid.density =   (float *) malloc(sizeof(float)*grid.nx*grid.nz);    
    for (i=0; i<grid.nx*grid.nz; i++) global_grid.density[i] = 0;
  }
  
  // main loop : go over the local particles 
  
  N = size_of_local_dataset;

  for (i=0; i < N; i++) 
  {
    // we need spatial coordinates of the particles (x,y,z)
    double x = pts->m_data[0][i];
    double y = pts->m_data[1][i];
    double z = pts->m_data[2][i];

    /* ----- for test purposes only
       double x = ((double)rand()/(double)RAND_MAX)*(grid.x1-grid.x0) + grid.x0;
       double y = grid.y0 + 0.5*grid.dy;
       double z = ((double)rand()/(double)RAND_MAX)*(grid.z1-grid.z0) + grid.z0;
    */

    // bin particles onto the grid
    ii = (int) ((x - grid.x0)/grid.dx);
    jj = (int) ((y - grid.y0)/grid.dy);
    kk = (int) ((z - grid.z0)/grid.dz);

    // 2D slice should suffice since the simulation should be statistically homogenious in y
    if (jj == 0 ) grid.density[ii + grid.nx*kk] ++;
    
  } // end of the loop over local particles
  
  // sum over all the processors
  MPI_Reduce(grid.density, global_grid.density, grid.nx*grid.nz, MPI_FLOAT, MPI_SUM, 0, comm);
  
  // output data on rank 0
  if (mpi_rank == 0) {
    FILE * f = fopen("./ne.gda","wb");
    int count = fwrite(global_grid.density,sizeof(float), global_grid.nx*global_grid.nz,f);
    if (count != global_grid.nx*global_grid.nz) printf("Error writing density file!\n");
    fclose(f);      
  }
  
  // all done !  
  free(grid.density);
  free(global_grid.density);  
}

