/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*										*/
/*   Files:         Have to list the files  					*/
/*										*/	
/*   Description:  BDATS: BigData Analytics at Trillion Scale    		*/ 
/*                                                                           	*/
/*   Author:  Md. Mostofa Ali Patwary                                        	*/
/*            Research Scientist, Parallel Computing Lab, Intel Corporation    	*/
/*            email: mostofa.ali.patwary@intel.com                          	*/
/*										*/
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

#ifndef _BDATS_DATA_STRUCT_
#define _BDATS_DATA_STRUCT_

#include "bdats_headers.h"

#define MASTER_NODE 0
#define NOT_ALIGNED_MALLOC
#define PARALLEL_FOR_CHUNK_SIZE 64
#define TOTAL_SAMPLES_PER_BILLION_FOR_MEDIAN 32 // change to smaller value later
#define RAND_SEED 1234567
#define PARTITIONING_COMMU_BUF 1
#define THREAD_LOCAL_BUFFER 1024
#define MPI_COMM_BEGIN_TAG 100 


#ifdef NOT_ALIGNED_MALLOC
#define BDATS_malloc(x) malloc(x)
#define BDATS_free(x) free(x) 
#define BDATS_realloc(x,y) realloc(x,y)
#else
#define BDATS_malloc(x) _mm_malloc(x, 64) 
#define BDATS_free(x) _mm_free(x) 
#endif

#define POW2(x) (1 << (x))


typedef float 	BDATS_decimal;
#define BDATS_decimal_mpi MPI_FLOAT

typedef int64_t BDATS_gIndex; 
#define BDATS_gIndex_mpi MPI_INT64_T

typedef int64_t BDATS_lIndex;

typedef struct
{
	BDATS_decimal 	m_lower;
	BDATS_decimal	m_upper;
}BoundingBox;

struct Points
{
	int64_t 	m_num_local_points; 	// local points count
	int64_t 	m_num_dims; 		// dimension of each point
	BDATS_decimal**	m_data;			// coordinates: 2D array, each D for one dimension data together
	
	int*		m_vec_prIDs;		// 32 bit should be good enough 
	BDATS_lIndex*	m_vec_local_IDs;	// could be 32 bit or 64 bit, assuming 64 bit
	BDATS_gIndex*	m_vec_global_IDs;	// 64 bit, no doubt  	

	BoundingBox*	m_bbox;		


	int64_t		m_num_global_points; 	
	
	int		m_mpi_comm_current_tag;
};

#endif
