#include "stdlib.h"
#include "hdf5.h"
#include "H5Part.h"
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#define     MAX_DATSET_COUNT 25		// Increase this based m_num_dims;
#define     OBJ_NAME_MAX 244		// Number of characters in different names
// #define	    DEBUG 1		// Uncomment to debug; prints the first 100 data values

// Structure to keep information of a dataset
typedef struct dataset_info{
  hid_t    open_file_id;    //file id returned by open
  char     name[244];       //dataset name, which also contains the group path
  hid_t    open_dataset_id; //dataset id returned by open
  hid_t    open_space_id;   //space id 
  hsize_t global_size;     //size of the dataset
  hsize_t local_size;      //local size for the dataset 
  hsize_t local_offset;    //offset 
  int      space_ndims;
  float   *buf;
  int      mpi_rank;
  int      mpi_size;
}dataset_info_t;

// DBScan specific data structure
typedef float BDATS_decimal;
typedef int64_t BDATS_gIndex;

typedef struct points_t {
        int64_t         m_num_global_points;    // number of points
        int64_t         m_num_local_points;     // number of points per this process
        int64_t         m_num_dims;             // dimension of each point
        BDATS_decimal   **m_data;               // 2D array; coordinates: size is m_num_local_points * m_num_dims,
	BDATS_gIndex   *m_vec_global_IDs;       // Global IDs to track points
}Points;

dataset_info_t  data_info_array[MAX_DATSET_COUNT];

Points pts;

void  open_datasets (dataset_info_t *data_info_array, Points *pts)
{
	hid_t   dset_id;
	hsize_t proc_offset;
	int     space_ndims;
	hid_t  dataspace;
	dset_id  = H5Dopen(data_info_array->open_file_id, data_info_array->name, H5P_DEFAULT);
	dataspace = H5Dget_space(dset_id);
	space_ndims = H5Sget_simple_extent_ndims(dataspace);
	
	proc_offset = data_info_array->mpi_rank * (pts->m_num_global_points/data_info_array->mpi_size);

	data_info_array->open_dataset_id = dset_id;
	data_info_array->local_offset    = proc_offset;
	data_info_array->global_size     = pts->m_num_global_points; 
	data_info_array->local_size      = pts->m_num_local_points;
	data_info_array->open_space_id   = dataspace;
	data_info_array->space_ndims     = space_ndims;
}

void read_segment(dataset_info_t * cur_dataset)
{
	hid_t plist2_id, memspace;

	memspace =  H5Screate_simple(cur_dataset->space_ndims, &(cur_dataset->local_size), NULL);

	plist2_id = H5Pcreate(H5P_DATASET_XFER);
	H5Pset_dxpl_mpio(plist2_id, H5FD_MPIO_COLLECTIVE);

	hsize_t my_offset, my_size;
	my_offset = cur_dataset->local_offset;
	my_size   = cur_dataset->local_size;
	H5Sselect_hyperslab(cur_dataset->open_space_id, H5S_SELECT_SET, &my_offset, NULL, &my_size , NULL);

	H5Dread(cur_dataset->open_dataset_id, H5T_NATIVE_FLOAT, memspace, cur_dataset->open_space_id, plist2_id, cur_dataset->buf);

	H5Pclose(plist2_id);
}

void get_metadata (char* filename, dataset_info_t *data_info_array, Points *pts, int mpi_rank, int mpi_size)
{
	int dataset_count;
	H5PartFile* H5P_fileid;
	H5P_fileid = H5PartOpenFileParallel(filename, H5PART_READ, MPI_COMM_WORLD);
	dataset_count = H5PartGetNumDatasets (H5P_fileid);

	int num_timesteps = H5PartGetNumSteps (H5P_fileid);

	// char step_name[OBJ_NAME_MAX];
	// if (H5PartReadStepAttrib(H5P_fileid, "filename", &step_name[0]) == 1)
	// {
		// printf("Read step from file: %s\n", step_name);
	// }

	// h5part_int64_t num_attribs = H5PartGetNumStepAttribs(H5P_fileid);
	// fprintf(stdout, "Number of step attributes in step #%lld: %lld\n", num_timesteps, (long long)num_attribs);

	int index;
	char   dataset_name[OBJ_NAME_MAX];

        pts->m_num_global_points = H5PartGetNumParticles (H5P_fileid);
	pts->m_num_dims = dataset_count;
        for(index=0; index < dataset_count; index++)
        {
		data_info_array[index].mpi_rank = mpi_rank;
		data_info_array[index].mpi_size = mpi_size;
		H5PartGetDatasetName(H5P_fileid, index, dataset_name, OBJ_NAME_MAX);
		// Replace the hard-coded time step name
		sprintf (data_info_array[index].name, "/Step#0/%s", dataset_name);
		// printf("\tDataset[%u] name= %s\n", index, data_info_array[index].name);
	}
	H5PartCloseFile(H5P_fileid);
}

void allocate_memory (Points *pts)
{
	BDATS_decimal** data_array;
	int dim;
	data_array = malloc (pts->m_num_dims * sizeof (BDATS_decimal*));
  	for (dim = 0; dim < pts->m_num_dims; dim++) {
		data_array[dim] = malloc (pts->m_num_local_points * sizeof (BDATS_decimal));
		if(data_array[dim] == NULL)
		{
			printf("Memory allocation for data_array[%d] failed... \n", dim);
			exit(-1);
		}
	}

	pts->m_data = data_array;
}

void read_driver (char *filename, Points *pts, MPI_Comm comm, MPI_Info info, int mpi_rank, int mpi_size)
{
	dataset_info_t *current_dataset;
	hid_t  file_id, plist_id;
	hsize_t		local_size;

	plist_id = H5Pcreate(H5P_FILE_ACCESS);
        H5Pset_fapl_mpio(plist_id, comm, info);
        file_id = H5Fopen(filename, H5F_ACC_RDONLY,plist_id);

	double t1, t2;
	MPI_Barrier(MPI_COMM_WORLD);
	t1 =  MPI_Wtime();

	int i, index;
	int64_t last_process_extra_pts = 0;

	// Assuming that each dataset has the same size
	last_process_extra_pts = pts->m_num_global_points % mpi_size;
        if (mpi_rank ==  (mpi_size - 1))
        {
                pts->m_num_local_points = pts->m_num_global_points/mpi_size + last_process_extra_pts;
        }
        else
        {
                pts->m_num_local_points = pts->m_num_global_points/mpi_size + last_process_extra_pts;
        }
	
	// Allocate memory for Indexes
	pts->m_vec_global_IDs =  (BDATS_gIndex*) malloc (pts->m_num_local_points * sizeof(BDATS_gIndex));
	if(pts->m_vec_global_IDs == NULL)
	{
		printf("Memory allocation for m_vec_global_IDs failed... \n");
		exit(-1);
	}

	int64_t global_offset = pts->m_num_local_points*mpi_rank;
	for (index = 0; index < pts->m_num_local_points; index++)
	{
		pts->m_vec_global_IDs[index] = global_offset + index;
	}

	// Allocate memory for data
	allocate_memory (pts);

	// Go through each HDF5 dataset and read data into certain locations.
	for(i = 0; i < pts->m_num_dims; i++)
	{
		current_dataset = &(data_info_array[i]);
		current_dataset->open_file_id = file_id;

		//Open datasets and set hyperslab boundaries
		open_datasets (current_dataset, pts);

		current_dataset->buf = *&pts->m_data[i];
		if(current_dataset->buf == NULL)
		{
			printf("Memory allocation for reading data fails ! \n");
			exit(-1);
		}

		//Parallel read
		read_segment(current_dataset);
		if (mpi_rank == 0)
			printf ("Finished reading dataset[%d] \n", i);
	}

	H5Pclose(plist_id);
	MPI_Barrier(MPI_COMM_WORLD);
	t2 =  MPI_Wtime();

	if(mpi_rank == 0 ){
		printf("Data read time %f \n", t2 - t1);
	}

	#ifdef DEBUG
		// Print the first 100 elements in each dataset
		int64_t j;
		for(i = 0 ; i < pts->m_num_dims; i++)
                {
                        printf ("\nDataset [%d]: \n", i);
                        for(j = 0; j < 100; j++)
                        {
				printf ("%f \t", pts->m_data[i][j]);
                        }
                }
		// Print the first 100 global indexes
		printf ("\nGlobal indexes of rank [%d]: ", mpi_rank);
		for (index = 0; index < 100; index++)
		{
			printf ("%lld \t", pts->m_vec_global_IDs[index]);
		}
		printf ("\n");
	#endif

	MPI_Barrier(MPI_COMM_WORLD);

	for(i = 0; i < pts->m_num_dims; i++)
	{
		current_dataset = &(data_info_array[i]);
		H5Sclose(current_dataset->open_space_id);
		H5Dclose(current_dataset->open_dataset_id);
	}

	H5Fclose(file_id);
	free (pts->m_data);
	free (pts->m_vec_global_IDs);
}

void  print_help(){
  char *msg="Usage: %s [OPTION] \n\
      	  -h help (--help)\n\
          -f name of the file (only HDF5 file in current version) \n\
          -d name of the dataset to read \n  \
          example: dbscan_read -f testf.h5p  \n  \
          example: dbscan_read -f testf.h5p  -d /Step#0/Energy -d /Step#0/x   -d /Step#0/y  -d /Step#0/z -d /Step#0/ux -d /Step#0/uy -d /Step#0/uz \n ";
   fprintf(stdout, msg, "dbscan_read");
}

int main(int argc, char *argv[])
{
	char   filename[OBJ_NAME_MAX], group[OBJ_NAME_MAX];
	int    i, dataset_count = 0;
	int    mpi_size, mpi_rank;

	MPI_Comm comm = MPI_COMM_WORLD;
	MPI_Info info = MPI_INFO_NULL;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(comm, &mpi_size);
	MPI_Comm_rank(comm, &mpi_rank);

	static const char *options="f:d:h";
	extern char *optarg;
	int c;
	while ((c = getopt(argc, argv, options)) != -1) 
	{
		switch (c) 
		{
			case 'f':
			case 'F': 
				strcpy(filename, optarg); 
			break;
			case 'd':
			case 'D': 
				strcpy(data_info_array[dataset_count].name, optarg); 
				data_info_array[dataset_count].mpi_rank = mpi_rank;
				data_info_array[dataset_count].mpi_size = mpi_size;
				dataset_count = dataset_count + 1;
			break;
			case 'h':
			case 'H':
				print_help();
			return 1;
			default: 
			break;
		} // switch
	} // while
	
	pts.m_num_dims = dataset_count;
	// If dataset names are not provided by the program,
		// get_metadata()
		// Assuming all the HDF5 datasets are to be read
	if (pts.m_num_dims == 0)
	{
		get_metadata (filename, data_info_array, &pts, mpi_rank, mpi_size);
	}

	// Read data
	read_driver (filename, &pts, comm, info, mpi_rank, mpi_size);

	MPI_Finalize();

	return 0;
}
