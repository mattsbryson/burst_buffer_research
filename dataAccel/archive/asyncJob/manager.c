#include "hdf5.h"


typedef int bool;
#define true 1
#define false 0

struct daFile
{
	hid_t file_id;
	char* filename;
	char* cacheFilename;
}


int main (int argc, char* argv[]) 
{
	FILE *actionLog = fopen("actionLog", "w+");
	
	hid_t file;
	
	while(fileOpenCheck(actionLog))
	{
		//get file ID, file info and save
		
		//create file in correct location, store info in struct
		
		//write done to log 
	}
	
	while(!fileCloseCheck(actionLog))
	{
		if(fileFlushCheck(actionLog))
		{
			//scan file for dataspaces
			
			//initialze that many mpi-io threads
			
			//have each thread copy a seperate dataspace
			
			//write done 
		}
		else
		{
			sleep(1);
		}
	}
	
	return 0;
}

//returns if file is flushed
bool fileFlushCheck(*actionLog)
{
	char line[256];
	bool flushed = false;
	
    while (fgets(line, sizeof(line), file)) 
	{
        /* note that fgets don't strip the terminating \n, checking its
           presence would allow to handle lines longer that sizeof(line) */
        //printf("%s", line); 
		
		if(strcmp(line, "flush") == 0)
		{
			fgets(line, sizeof(line), file);
			if(strcmp(line, "done") != 0)
			{
				flushed = true;
			}
		}
    }
	return flushed;
}

bool fileCloseCheck(*actionLog)
{
	char line[256];
	bool close = false;
	
    while (fgets(line, sizeof(line), file)) 
	{
        /* note that fgets don't strip the terminating \n, checking its
           presence would allow to handle lines longer that sizeof(line) */
        //printf("%s", line); 
		
		if(strcmp(line, "close") == 0)
		{
			fgets(line, sizeof(line), file);
			if(strcmp(line, "done") != 0)
			{
				close = true;
			}
		}
    }
	return close;
}

bool fileOpenCheck(*actionLog)
{
	char line[256];
	bool open = false;
	
    while (fgets(line, sizeof(line), file)) 
	{
        /* note that fgets don't strip the terminating \n, checking its
           presence would allow to handle lines longer that sizeof(line) */
        //printf("%s", line); 
		
		if(strcmp(line, "open") == 0)
		{
			fgets(line, sizeof(line), file);
			if(strcmp(line, "done") != 0)
			{
				open = true;
			}
		}
    }
	return open;
}

hid_t getFileID(*actionLog)
{
	hid_t file;
	int iFile = -1;
	
	char line[256];
	
    while (fgets(line, sizeof(line), file)) 
	{
        /* note that fgets don't strip the terminating \n, checking its
           presence would allow to handle lines longer that sizeof(line) */
        //printf("%s", line); 
		
		if((strcmp(line, "flush") == 0) || (strcmp(line, "open") == 0) || (strcmp(line, "open") == 0))
		{
			fgets(line, sizeof(line), file);
			if(strcmp(line, "done") != 0)
			{
				sscanf(line, %d, &iFile);
				file = iFile;
			}
		}
    }
	if(iFile == -1)
	{
		return -1;
	}
	else
	{
		return file;
	}
}

char* getFileName(*actionLog)
{
	hid_t file;
	int iFile = -1;
	
	char line[256];
	
    while (fgets(line, sizeof(line), file)) 
	{
        /* note that fgets don't strip the terminating \n, checking its
           presence would allow to handle lines longer that sizeof(line) */
        //printf("%s", line); 
		
		if((strcmp(line, "flush") == 0) || (strcmp(line, "close") == 0))
		{
			fgets(line, sizeof(line), file);
			if(strcmp(line, "done") != 0)
			{
				sscanf(line, %d, &iFile);
				file = iFile;
			}
		}
    }
	if(iFile == -1)
	{
		return -1;
	}
	else
	{
		return file;
	}
}