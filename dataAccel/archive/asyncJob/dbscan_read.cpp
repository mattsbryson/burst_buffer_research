#include "bdats_h5reader.h"

void  print_help(){
  char *msg="Usage: %s [OPTION] \n\
      	  -h help (--help)\n\
          -f name of the file (only HDF5 file in current version) \n\
          -d name of the dataset to read \n  \
          example: dbscan_read -f testf.h5p  -o outfile.h5 p\n  \
          example: dbscan_read -f testf.h5p  -d /Step#0/Energy -d /Step#0/x   -d /Step#0/y  -d /Step#0/z -d /Step#0/ux -d /Step#0/uy -d /Step#0/uz \n";
   fprintf(stdout, msg, "dbscan_read");
}

int main(int argc, char *argv[])
{
	char   filename[OBJ_NAME_MAX], group[OBJ_NAME_MAX], outfilename[OBJ_NAME_MAX];
	int    i, dataset_count = 0;
	int    mpi_size, mpi_rank;

	MPI_Comm comm = MPI_COMM_WORLD;
	MPI_Info info = MPI_INFO_NULL;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(comm, &mpi_size);
	MPI_Comm_rank(comm, &mpi_rank);


	dataset_info_t  data_info_array[MAX_DATSET_COUNT];
	Points pts;

	static const char *options="f:d:o:h";
	extern char *optarg;
	int c;
	while ((c = getopt(argc, argv, options)) != -1) 
	{
		switch (c) 
		{
			case 'f':
			case 'F': 
				strcpy(filename, optarg); 
			break;
			case 'd':
			case 'D': 
				strcpy(data_info_array[dataset_count].name, optarg); 
				data_info_array[dataset_count].mpi_rank = mpi_rank;
				data_info_array[dataset_count].mpi_size = mpi_size;
				dataset_count = dataset_count + 1;
			break;
			case 'o': 
			case 'O': 
				strcpy(outfilename, optarg); 
			break;
			case 'h':
			case 'H':
				print_help();
			return 1;
			default: 
			break;
		} // switch
	} // while
	
	// Read data: if dataset_count = 0, the reader will read all columns. 
	read_hdf5_file(filename, &pts, data_info_array, dataset_count);

	char *dataset_name[2] = {"vec_global_IDs", "vec_cluster_IDs"};

	H5PartFile *h5fileid;
	h5fileid = init_h5outfile (outfilename);
	write_dataset_BDATS_gIndex (dataset_name[0], pts.m_vec_global_IDs, pts.m_num_local_points, h5fileid);
	write_dataset_BDATS_gIndex (dataset_name[1], pts.m_vec_global_IDs, pts.m_num_local_points, h5fileid);
	close_h5outfile (h5fileid);

	MPI_Finalize();

	return 0;
}
