//Provides all file operations without actually fully supporting them. Upon choosing a file library, this library just passes the requests along. All that needs to be changed in the host application is the way requests are passed.

//implemented it so it just passes back a new hdf5 file handle that goes in the burst buffer directory, while queing the read/write requests



#include "hdf5.h"



//TODO change location of cache file name, or allowed to be added by job script
hid_t DA_H5Fcreate(const char *cacheFilename, const char *filename, unsigned flags, hid_t fcpl_id, hid_t fapl_id)
{
	hid_t action = H5Fcreate(cacheFilename, flags, fcpl_id, fapl_id);
	
	FILE *actionLog = fopen("actionLog", "w");
	// overly complex - fprintf(actionLog, "hid:%d,cache:%s,final:%s\n", hid_t, cacheFilename, filename);
	//prints action, then filename, then cacheFilename, then hid_t
	fprintf(actionLog, "%s\n", "open");
	fprintf(actionLog, "%d\n", hid_t);
	fprintf(actionLog, "%s\n", filename);
	fprintf(actionLog, "%s\n", cacheFilename);
	
	fclose(actionLog);
	
	return action;
 
 }

herr_t DA_H5Fflush(hid_t object_id, H5F_scope_t scope)
{
	herr_t action = H5Fflush(object_id, scope);
	
	FILE *actionLog = fopen("actionLog", "a+");
	fprintf(actionLog, "%s\n%d\n", "flush", object_id);
	fclose(actionLog);
	
	return action; 
}

herr_t DA_H5Fclose(hid_t file_id, H5F_scope_t scope)
{
	herr_t action1 = H5Fflush(object_id, scope);
	herr_t action = H5Fclose(object_id);
	
	FILE *actionLog = fopen("actionLog", "a+");
	fprintf(actionLog, "%s\n%d\n", "close", object_id);
	fclose(actionLog);
	
	return action; 
}












