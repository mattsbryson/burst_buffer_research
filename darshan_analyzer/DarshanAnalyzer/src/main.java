import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class main 
{
	public static void main(String[] args) throws IOException, InterruptedException 
	{
		//globals
		int posixStart = 1;
		int mpiStart = 12;
		
		FileWriter writer = new FileWriter("darshan.csv");
		String [] header = {
				"Job ID",
				"POSIX_BYTES_WRITTEN",
				"POSIX_F_OPEN_TIMESTAMP",
				"POSIX_F_WRITE_START_TIMESTAMP",
				"POSIX_F_WRITE_END_TIMESTAMP",
				"POSIX_F_CLOSE_TIMESTAMP",
				"POSIX_F_MAX_WRITE_TIME",
				"POSIX_F_WRITE_RANK_TIME",
				"POSIX_F_VARIANCE_RANK_WRITE",
				"POSIX_F_SLOWEST_RANK_TIME",
				"POSIX_F_FASTEST_RANK_TIME",
				"POSIX_MAX_VARIANCE", //computed value
				"MPIIO_BYTES_WRITTEN",
				"MPIIO_F_OPEN_TIMESTAMP",
				"MPIIO_F_WRITE_START_TIMESTAMP",
				"MPIIO_F_WRITE_END_TIMESTAMP",
				"MPIIO_F_CLOSE_TIMESTAMP",
				"MPIIO_F_MAX_WRITE_TIME",
				"MPIIO_F_WRITE_RANK_TIME",
				"MPIIO_F_VARIANCE_RANK_WRITE",
				"MPIIO_F_SLOWEST_RANK_TIME",
				"MPIIO_F_FASTEST_RANK_TIME",
				"MPIIO_MAX_VARIANCE"}; //computed value
		
		
		/*writer.append("JobID,POSIX_BYTES_WRITTEN,POSIX_F_OPEN_TIMESTAMP,POSIX_F_WRITE_TIMESTAMP,POSIX_F_WRITE_END_TIMESTAMP,POSIX_F_CLOSE_TIMESTAMP,"
				+ "POSIX_F_MAX_WRITE_TIME,POSIX_F_VARIANCE_RANK_TIME,"
				+"MPIIO_BYTES_WRITTEN,MPIIO_F_OPEN_TIMESTAMP,MPIIO_F_WRITE_START_TIMESTAMP,MPIIO_F_WRITE_END_TIMESTAMP,MPIIO_F_CLOSE_TIMESTAMP,"
				+ "MPIIO_F_MAX_WRITE_TIME,MPIIO_F_WRITE_RANK_TIME,MPIIO_F_VARIANCE_RANK_TIME\n");
		*/
		
		//System.out.println("header length" + header.length);
		
		for(int y = 0; y < header.length; y++)
		{
			if(y<header.length-1)
			{
				writer.append(header[y] + ",");
			}
			else
			{
				writer.append(header[y] + "\n");
			}
				
		}
		
		
		
		//get logs into pwd using previously written shell script
		Process start = Runtime.getRuntime().exec("chmod +x /global/homes/m/mbryson/mbryson_data/burstb/darshan_analyzer/darshan_mover");
		start.waitFor();
		Process p = Runtime.getRuntime().exec("/global/homes/m/mbryson/mbryson_data/burstb/darshan_analyzer/darshan_mover");
		p.waitFor();
		//Runtime.getRuntime().exec("chmod +x darshan_mover");
		//get all files 
		File folder = new File("/global/homes/m/mbryson/mbryson_data/burstb/darshan");
		File[] listOfFiles = folder.listFiles();
		for (int i = 0; i < listOfFiles.length; i++)
		{
			if (listOfFiles[i].isFile()) 
			{
				int jobID = 0;
				
				long POSIX_BYTES_WRITTEN = 0;
				double POSIX_F_OPEN_TIMESTAMP = -1;
				double POSIX_F_START_TIMESTAMP = -1;
				double POSIX_F_END_TIMESTAMP = -1;
				double POSIX_F_CLOSE_TIMESTAMP = -1;
				double POSIX_F_MAX_TIME = -1;
				double POSIX_F_RANK_TIME = -1;
				double POSIX_F_VARIANCE_RANK = -1;		
				double POSIX_F_SLOWEST_RANK_TIME = -1;
				double POSIX_F_FASTEST_RANK_TIME = -1;
				
				long MPIIO_BYTES_WRITTEN = 0;
				double MPIIO_F_OPEN_TIMESTAMP = -1;
				double MPIIO_F_START_TIMESTAMP = -1;
				double MPIIO_F_END_TIMESTAMP = -1;
				double MPIIO_F_CLOSE_TIMESTAMP = -1;
				double MPIIO_F_MAX_TIME = -1;
				double MPIIO_F_RANK_TIME = -1;
				double MPIIO_F_VARIANCE_RANK = -1;		
				double MPIIO_F_SLOWEST_RANK_TIME = -1;
				double MPIIO_F_FASTEST_RANK_TIME = -1;
				
				//computed values
				double POSIX_MAX_VARIANCE = -1;
				double MPIIO_MAX_VARIANCE = -1;
				

				Scanner reader = new Scanner(listOfFiles[i]);
				String s;
				double [] results = new double[header.length];
				while(reader.hasNext())
				{
					if((s = reader.next()).equals("POSIX_BYTES_WRITTEN"))
					{
						//System.out.println(s);
						//System.out.println(reader.nextLong());
						POSIX_BYTES_WRITTEN = reader.nextLong();
						results[posixStart + 0] = POSIX_BYTES_WRITTEN;
					}
					else if(s.equals("POSIX_F_OPEN_TIMESTAMP"))
					{
						POSIX_F_OPEN_TIMESTAMP = reader.nextDouble();
						results[posixStart + 1] = POSIX_F_OPEN_TIMESTAMP;
					}
					else if(s.equals("POSIX_F_WRITE_START_TIMESTAMP"))
					{
						POSIX_F_START_TIMESTAMP = reader.nextDouble();
						results[posixStart + 2] = POSIX_F_START_TIMESTAMP;
					}
					else if(s.equals("POSIX_F_WRITE_END_TIMESTAMP"))
					{
						POSIX_F_END_TIMESTAMP = reader.nextDouble();
						results[posixStart + 3] = POSIX_F_END_TIMESTAMP;
					}
					else if(s.equals("POSIX_F_CLOSE_TIMESTAMP"))
					{
						POSIX_F_CLOSE_TIMESTAMP = reader.nextDouble();
						results[posixStart + 4] = POSIX_F_CLOSE_TIMESTAMP;
					}
					else if(s.equals("POSIX_F_MAX_WRITE_TIME"))
					{
						POSIX_F_MAX_TIME = reader.nextDouble();
						results[posixStart + 5] = POSIX_F_MAX_TIME;
					}
					else if(s.equals("POSIX_F_WRITE_TIME"))
					{
						POSIX_F_RANK_TIME = reader.nextDouble();
						results[posixStart + 6] = POSIX_F_RANK_TIME;
					}
					else if(s.equals("POSIX_F_VARIANCE_RANK_TIME"))
					{
						POSIX_F_VARIANCE_RANK = reader.nextDouble();
						results[posixStart + 7] = POSIX_F_VARIANCE_RANK;
					}
					else if(s.equals("POSIX_F_SLOWEST_RANK_TIME"))
					{
						POSIX_F_SLOWEST_RANK_TIME = reader.nextDouble();
						results[posixStart + 8] = POSIX_F_SLOWEST_RANK_TIME;
					}
					else if(s.equals("POSIX_F_FASTEST_RANK_TIME"))
					{
						POSIX_F_FASTEST_RANK_TIME = reader.nextDouble();
						results[posixStart + 9] = POSIX_F_FASTEST_RANK_TIME;
					}
					//POSIX-10 in use by comp
					//start of MPIIO
					else if(s.equals("MPIIO_BYTES_WRITTEN"))
					{
						//System.out.println(s);
						//System.out.println(reader.nextLong());
						MPIIO_BYTES_WRITTEN = reader.nextLong();
						results[mpiStart + 0] = MPIIO_BYTES_WRITTEN;
					}
					else if(s.equals("MPIIO_F_OPEN_TIMESTAMP"))
					{
						MPIIO_F_OPEN_TIMESTAMP = reader.nextDouble();
						results[mpiStart + 1] = MPIIO_F_OPEN_TIMESTAMP;
					}
					else if(s.equals("MPIIO_F_WRITE_START_TIMESTAMP"))
					{
						MPIIO_F_START_TIMESTAMP = reader.nextDouble();
						results[mpiStart + 2] = MPIIO_F_START_TIMESTAMP;
					}
					else if(s.equals("MPIIO_F_WRITE_END_TIMESTAMP"))
					{
						MPIIO_F_END_TIMESTAMP = reader.nextDouble();
						results[mpiStart + 3] = MPIIO_F_END_TIMESTAMP;
					}
					else if(s.equals("MPIIO_F_CLOSE_TIMESTAMP"))
					{
						MPIIO_F_CLOSE_TIMESTAMP = reader.nextDouble();
						results[mpiStart + 4] = MPIIO_F_CLOSE_TIMESTAMP;
					}
					else if(s.equals("MPIIO_F_MAX_WRITE_TIME"))
					{
						MPIIO_F_MAX_TIME = reader.nextDouble();
						results[mpiStart + 5] = MPIIO_F_MAX_TIME;
					}
					else if(s.equals("MPIIO_F_WRITE_TIME"))
					{
						MPIIO_F_RANK_TIME = reader.nextDouble();
						results[mpiStart + 6] = MPIIO_F_RANK_TIME;
					}
					else if(s.equals("MPIIO_F_VARIANCE_RANK_TIME"))
					{
						MPIIO_F_VARIANCE_RANK = reader.nextDouble();
						results[mpiStart + 7] = MPIIO_F_VARIANCE_RANK;
						//System.out.println(MPIIO_F_VARIANCE_RANK);
					}
					else if(s.equals("MPIIO_F_SLOWEST_RANK_TIME"))
					{
						MPIIO_F_SLOWEST_RANK_TIME = reader.nextDouble();
						results[mpiStart + 8] = MPIIO_F_SLOWEST_RANK_TIME;
					}
					else if(s.equals("MPIIO_F_FASTEST_RANK_TIME"))
					{
						MPIIO_F_FASTEST_RANK_TIME = reader.nextDouble();
						results[mpiStart + 9] = MPIIO_F_FASTEST_RANK_TIME;
					}
					//MPI-10 in use by comp
					else if(s.equals("jobid:"))
					{
						jobID = reader.nextInt();
						results[0] = jobID;
					}
				}
				//computed values
				POSIX_MAX_VARIANCE = Math.abs(POSIX_F_FASTEST_RANK_TIME - POSIX_F_SLOWEST_RANK_TIME);
				results[posixStart + 10] = POSIX_MAX_VARIANCE;
				MPIIO_MAX_VARIANCE = Math.abs(MPIIO_F_FASTEST_RANK_TIME - MPIIO_F_SLOWEST_RANK_TIME);
				results[mpiStart + 10] = MPIIO_MAX_VARIANCE;
				
				
				for(int y = 0; y < results.length; y++)
				{
					if(y<results.length-1)
					{
						writer.append(results[y] + ",");
						/*
						if(results[y] == 0)
						{
							System.out.println("Zero result @ " + y);
						}
						*/
					}
					else
					{
						writer.append(results[y] + "\n");
					}
						
				}
				
				//System.out.println("File " + listOfFiles[i].getName());
			} 
			else if (listOfFiles[i].isDirectory()) {
		        //System.out.println("Directory " + listOfFiles[i].getName());
			}
		}
		
		writer.flush();
		writer.close();
	}

}

/*
#Variables that are grabbed 
#POSIX
#   POSIX_BYTES_WRITTEN: total bytes read and written.
#   POSIX_F_OPEN_TIMESTAMP: timestamp of first open.
#   POSIX_F_WRITE_START_TIMESTAMP: timestamp of first read/write.
#   POSIX_F_WRITE_END_TIMESTAMP: timestamp of last read/write.
#   POSIX_F_CLOSE_TIMESTAMP: timestamp of last close.
#   POSIX_F_MAX_WRITE_TIME: duration of the slowest read and write operations.
#   POSIX_F_WRITE_RANK_TIME: fastest and slowest I/O time for a single rank (for shared files).
#   POSIX_F_VARIANCE_RANK_WRITE: variance of total I/O time and bytes moved for all ranks (for shared files).

#MPIIOIO
#  MPIIO_BYTES_WRITTEN: total bytes read and written at MPIIO-IO layer.
#  MPIIO_F_OPEN_TIMESTAMP: timestamp of first open.
#  MPIIO_F_WRITE_START_TIMESTAMP: timestamp of first MPIIO-IO read/write.
#  MPIIO_F_WRITE_END_TIMESTAMP: timestamp of last MPIIO-IO read/write.
#  MPIIO_F_CLOSE_TIMESTAMP: timestamp of last close.
#  MPIIO_F_MAX_WRITE_TIME: duration of the slowest MPIIO-IO read and write operations.
#  MPIIO_F_WRITE_RANK_TIME: fastest and slowest I/O time for a single rank (for shared files).
#  MPIIO_F_VARIANCE_RANK_WRITE: variance of total I/O time and bytes moved for all ranks (for shared files).


Other variance ranks also included, see header for details
*/