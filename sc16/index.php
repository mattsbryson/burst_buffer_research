<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">


<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
<meta http-equiv="Content-Style-Type" content="text/css"/>

<title>Matt Bryson</title>
<link href="styles/trs-style-iWeblike.css" rel="stylesheet" type="text/css" />
</head>


<body>

<div id="navigation">
<table>
<tr>
<td><a href="index.php">Home</a></td>
<td><a href="about.php">About</a></td>
<td><a href="research.php">Research</a></td>
<td><a href="other.php">Other</a></td>
</tr>
</table>


<div class="hr"> <hr /> </div>




</div>   <! End of navigation div -->

<div id="name">
Matt Bryson
</div>

<div id="main">



<img src="images-static/bryson_matt.jpg" width="200" >
<div id="right">

<table>
<tr><td>Ph.D. Graduate Student</td></tr>
<tr><td>Storage Systems Research Center (SSRC)</td></tr>
<tr><td>Jack Baskin School of Engineering</td></tr>
<tr> <td>University of California, Santa Cruz</td></tr>
</table>

<table>
<tr><td><bold>Email:</bold> mbryson *at* ucsc *dot* edu </td> </tr>
<tr><td><bold>Phone:</bold> (805) 501-2312</td> </tr>
<tr><td><bold>Office:</bold> E2-383</td> </tr>
</tr>
</table>

<table>
<tr><td><a href="https://users.soe.ucsc.edu/~mbryson/docs/cv_mattbryson.pdf"> <bold>Curriculum Vitae (CV)</bold> </a> </td> </tr>
<tr><td><a href="https://users.soe.ucsc.edu/~mbryson/docs/resume_mattbryson.pdf"> <bold>Resume</bold> </a> </td> </tr>
</tr>
</table>

</div>

<div id="footer">

<br clear="all" />


<table cellpadding="2">
<tr>
  <td ><i>Last modified on <? echo  date("F d, Y", filemtime($_SERVER["SCRIPT_FILENAME"])) ?>.</i></td>
</tr>
</table>

</div>
</div>









</body>
</html>


