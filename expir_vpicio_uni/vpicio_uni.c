/****** Copyright Notice ***
 *
 * PIOK - Parallel I/O Kernels - VPIC-IO, VORPAL-IO, and GCRM-IO, Copyright
 * (c) 2015, The Regents of the University of California, through Lawrence
 * Berkeley National Laboratory (subject to receipt of any required
 * approvals from the U.S. Dept. of Energy).  All rights reserved.
 *
 * If you have questions about your rights to use or distribute this
 * software, please contact Berkeley Lab's Innovation & Partnerships Office
 * at  IPO@lbl.gov.
 *
 * NOTICE.  This Software was developed under funding from the U.S.
 * Department of Energy and the U.S. Government consequently retains
 * certain rights. As such, the U.S. Government has been granted for itself
 * and others acting on its behalf a paid-up, nonexclusive, irrevocable,
 * worldwide license in the Software to reproduce, distribute copies to the
 * public, prepare derivative works, and perform publicly and display
 * publicly, and to permit other to do so.
 *
 ****************************/

/**
 *
 * Email questions to SByna@lbl.gov
 * Scientific Data Management Research Group
 * Lawrence Berkeley National Laboratory
 *
*/


// Description: This is a simple benchmark based on VPIC's I/O interface
//		Each process writes a specified number of particles into 
//		a h5part output file
// Author:	Suren Byna <SByna@lbl.gov>
//		Lawrence Berkeley National Laboratory, Berkeley, CA
// 


#include "H5Part.h"
#include "H5Block.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// A simple timer based on gettimeofday
#include "./timer.h"
struct timeval start_time[3];
float elapse[3];

H5PartFile* file0;
H5PartFile* file1;
H5PartFile* file2;
H5PartFile* file3;
H5PartFile* file4;
H5PartFile* file5;

int numparticles = 8388608;	// 8  meg particles per process
float *x, *y, *z;
float *px, *py, *pz;
h5part_int32_t *id1, *id2;
int x_dim = 64;
int y_dim = 64; 
int z_dim = 64;

// Uniform random number
inline double uniform_random_number() 
{
	return (((double)rand())/((double)(RAND_MAX)));
}

// Initialize particle data
void init_particles ()
{
	int i;
	for (i=0; i<numparticles; i++) 
	{
		id1[i] = i;
		id2[i] = i*2;
		x[i] = uniform_random_number()*x_dim;
		y[i] = uniform_random_number()*y_dim;
		z[i] = ((double)id1[i]/numparticles)*z_dim;    
		px[i] = uniform_random_number()*x_dim;
		py[i] = uniform_random_number()*y_dim;
		pz[i] = ((double)id2[i]/numparticles)*z_dim;    
	}
}

// Create H5Part file and write data
void create_and_write_synthetic_h5part_data(int rank) 
{
	// Note: printf statements are inserted basically 
	// to check the progress. Other than that they can be removed
	H5PartSetStep(file0, 0); // only 1 timestep in this file
	H5PartSetStep(file1, 0); // only 1 timestep in this file
	H5PartSetStep(file2, 0); // only 1 timestep in this file
	H5PartSetStep(file3, 0); // only 1 timestep in this file
	H5PartSetStep(file4, 0); // only 1 timestep in this file
	H5PartSetStep(file5, 0); // only 1 timestep in this file
	if (rank == 0) printf ("Step is set \n");
	H5PartSetNumParticles(file0, numparticles);
	H5PartSetNumParticles(file1, numparticles);
	H5PartSetNumParticles(file2, numparticles);
	H5PartSetNumParticles(file3, numparticles);
	H5PartSetNumParticles(file4, numparticles);
	H5PartSetNumParticles(file5, numparticles);
	if (rank == 0) printf ("Num particles is set \n");
	float var_data_size;

	timer_on (2);
	H5PartWriteDataFloat32(file0,"x",x); 
	H5PartWriteDataFloat32(file1,"x",x);
	H5PartWriteDataFloat32(file2,"x",x);
	H5PartWriteDataFloat32(file3,"x",x);
	H5PartWriteDataFloat32(file4,"x",x);
	H5PartWriteDataFloat32(file5,"x",x);
	timer_off (2);
	var_data_size = numparticles * sizeof (h5part_float32_t)/(1024*1024);
	printf("Rank: %d, wrote %.2f MB, time: %f sec; rate: %f\n", rank, var_data_size, elapse[2], var_data_size/elapse[2]);
	timer_reset(2);
	if (rank == 0) printf ("Written variable 1 \n");

	timer_on (2);
	H5PartWriteDataFloat32(file0,"y",y);
	H5PartWriteDataFloat32(file1,"y",y);
	H5PartWriteDataFloat32(file2,"y",y);
	H5PartWriteDataFloat32(file3,"y",y);
	H5PartWriteDataFloat32(file4,"y",y);
	H5PartWriteDataFloat32(file5,"y",y);
	if (rank == 0) printf ("Written variable 2 \n");
	timer_off (2);
	var_data_size = numparticles * sizeof (h5part_float32_t)/(1024*1024);
	printf("Rank: %d, wrote %.2f MB, time: %f sec; rate: %f\n", rank, var_data_size, elapse[2], var_data_size/elapse[2]);
	timer_reset(2);

	timer_on (2);
	H5PartWriteDataFloat32(file0,"z",z);
	H5PartWriteDataFloat32(file1,"z",z);
	H5PartWriteDataFloat32(file2,"z",z);
	H5PartWriteDataFloat32(file3,"z",z);
	H5PartWriteDataFloat32(file4,"z",z);
	H5PartWriteDataFloat32(file5,"z",z);
	if (rank == 0) printf ("Written variable 3 \n");
	timer_off (2);
	var_data_size = numparticles * sizeof (h5part_float32_t)/(1024*1024);
	printf("Rank: %d, wrote %.2f MB, time: %f sec; rate: %f\n", rank, var_data_size, elapse[2], var_data_size/elapse[2]);
	timer_reset(2);

	timer_on (2);
	H5PartWriteDataInt32(file0,"id1",id1);
	H5PartWriteDataInt32(file1,"id1",id1);
	H5PartWriteDataInt32(file2,"id1",id1);
	H5PartWriteDataInt32(file3,"id1",id1);
	H5PartWriteDataInt32(file4,"id1",id1);
	H5PartWriteDataInt32(file5,"id1",id1);
	if (rank == 0) printf ("Written variable 4 \n");
	timer_off (2);
	var_data_size = numparticles * sizeof (h5part_int32_t)/(1024*1024);
	printf("Rank: %d, wrote %.2f MB, time: %f sec; rate: %f\n", rank, var_data_size, elapse[2], var_data_size/elapse[2]);
	timer_reset(2);

	timer_on (2);
	H5PartWriteDataInt32(file0,"id2",id2);
	H5PartWriteDataInt32(file1,"id2",id2);
	H5PartWriteDataInt32(file2,"id2",id2);
	H5PartWriteDataInt32(file3,"id2",id2);
	H5PartWriteDataInt32(file4,"id2",id2);
	H5PartWriteDataInt32(file5,"id2",id2);
	if (rank == 0) printf ("Written variable 5 \n");
	timer_off (2);
	var_data_size = numparticles * sizeof (h5part_int32_t)/(1024*1024);
	printf("Rank: %d, wrote %.2f MB, time: %f sec; rate: %f\n", rank, var_data_size, elapse[2], var_data_size/elapse[2]);
	timer_reset(2);

	timer_on (2);
	H5PartWriteDataFloat32(file0,"px",px); 
	H5PartWriteDataFloat32(file1,"px",px); 
	H5PartWriteDataFloat32(file2,"px",px); 
	H5PartWriteDataFloat32(file3,"px",px); 
	H5PartWriteDataFloat32(file4,"px",px); 
	H5PartWriteDataFloat32(file5,"px",px); 
	if (rank == 0) printf ("Written variable 6 \n");
	timer_off (2);
	var_data_size = numparticles * sizeof (h5part_float32_t)/(1024*1024);
	printf("Rank: %d, wrote %.2f MB, time: %f sec; rate: %f\n", rank, var_data_size, elapse[2], var_data_size/elapse[2]);
	timer_reset(2);

	timer_on (2);
	H5PartWriteDataFloat32(file0,"py",py);
	H5PartWriteDataFloat32(file1,"py",py);
	H5PartWriteDataFloat32(file2,"py",py);
	H5PartWriteDataFloat32(file3,"py",py);
	H5PartWriteDataFloat32(file4,"py",py);
	H5PartWriteDataFloat32(file5,"py",py);
	if (rank == 0) printf ("Written variable 7 \n");
	timer_off (2);
	var_data_size = numparticles * sizeof (h5part_float32_t)/(1024*1024);
	printf("Rank: %d, wrote %.2f MB, time: %f sec; rate: %f\n", rank, var_data_size, elapse[2], var_data_size/elapse[2]);
	timer_reset(2);

	timer_on (2);
	H5PartWriteDataFloat32(file0,"pz",pz);
	H5PartWriteDataFloat32(file1,"pz",pz);
	H5PartWriteDataFloat32(file2,"pz",pz);
	H5PartWriteDataFloat32(file3,"pz",pz);
	H5PartWriteDataFloat32(file4,"pz",pz);
	H5PartWriteDataFloat32(file5,"pz",pz);
	if (rank == 0) printf ("Written variable 8 \n");
	timer_off (2);
	var_data_size = numparticles * sizeof (h5part_float32_t)/(1024*1024);
	printf("Rank: %d, wrote %.2f MB, time: %f sec; rate: %f\n", rank, var_data_size, elapse[2], var_data_size/elapse[2]);
	timer_reset(2);
}

int main (int argc, char* argv[]) 
{
	char *out_dir = argv[1];
	
	char *file0_name;
	char *file1_name;
	char *file2_name;
	char *file3_name;
	char *file4_name;
	char *file5_name;
	
	char *fname0 = "sample0.h5part";
	char *fname1 = "sample1.h5part";
	char *fname2 = "sample2.h5part";
	char *fname3 = "sample3.h5part";
	char *fname4 = "sample4.h5part";
	char *fname5 = "sample5.h5part";
	
	MPI_Init(&argc,&argv);
	int my_rank, num_procs;
	int rank;
	MPI_Comm_rank (MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size (MPI_COMM_WORLD, &num_procs);
	
	rank = my_rank;

	char srank[100000];
	sprintf(srank, "%d", my_rank); 
	
	if((file0_name = malloc(strlen(out_dir)+strlen(fname0)+1)) != NULL){
	    file0_name[0] = '\0';   // ensures the memory is an empty string
	    strcat(file0_name,out_dir);
	    strcat(file0_name,fname0);
	} else {
	    printf("malloc failed!\n");
	    // exit?
	}
	printf("%s\n", file0_name);
	
	if((file1_name = malloc(strlen(out_dir)+strlen(fname0)+1)) != NULL){
	    file1_name[0] = '\0';   // ensures the memory is an empty string
	    strcat(file1_name,out_dir);
	    strcat(file1_name,fname0);
	} else {
	    printf("malloc failed!\n");
	    // exit?
	}
	printf("%s\n", file1_name);
	
	if((file2_name = malloc(strlen(out_dir)+strlen(fname0)+1)) != NULL){
	    file2_name[0] = '\0';   // ensures the memory is an empty string
	    strcat(file2_name,out_dir);
	    strcat(file2_name,fname0);
	} else {
	    printf("malloc failed!\n");
	    // exit?
	}
	printf("%s\n", file2_name);
	
	if((file3_name = malloc(strlen(out_dir)+strlen(fname0)+1)) != NULL){
	    file3_name[0] = '\0';   // ensures the memory is an empty string
	    strcat(file3_name,out_dir);
	    strcat(file3_name,fname0);
	} else {
	    printf("malloc failed!\n");
	    // exit?
	}
	printf("%s\n", file3_name);
	
	if((file4_name = malloc(strlen(out_dir)+strlen(fname0)+1)) != NULL){
	    file4_name[0] = '\0';   // ensures the memory is an empty string
	    strcat(file4_name,out_dir);
	    strcat(file4_name,fname0);
	} else {
	    printf("malloc failed!\n");
	    // exit?
	}
	printf("%s\n", file4_name);
	
	if((file5_name = malloc(strlen(out_dir)+strlen(fname0)+1)) != NULL){
	    file5_name[0] = '\0';   // ensures the memory is an empty string
	    strcat(file5_name,out_dir);
	    strcat(file5_name,fname0);
	} else {
	    printf("malloc failed!\n");
	    // exit?
	}
	printf("%s\n", file5_name);

	if (argc == 3)
	{
		numparticles = (atoi (argv[2]))*1024*1024;
	}
	else
	{
		numparticles = 32*1024*1024;
	}

	if (rank == 0) printf("Number of paritcles: %ld \n", numparticles);

	x=(float*)malloc(numparticles*sizeof(double));
	y=(float*)malloc(numparticles*sizeof(double));
	z=(float*)malloc(numparticles*sizeof(double));

	px=(float*)malloc(numparticles*sizeof(double));
	py=(float*)malloc(numparticles*sizeof(double));
	pz=(float*)malloc(numparticles*sizeof(double));

	id1=(h5part_int32_t*)malloc(numparticles*sizeof(h5part_int32_t));
	id2=(h5part_int32_t*)malloc(numparticles*sizeof(h5part_int32_t));

	init_particles ();

	if (rank == 0)
	{
		printf ("Finished initializing particles \n");
	}

	MPI_Barrier (MPI_COMM_WORLD);
	timer_on (0);
	//file = H5PartOpenFileParallelAlign(file_name, H5PART_WRITE, MPI_COMM_WORLD, alignf);
	// file = H5PartOpenFileParallel (file_name, H5PART_WRITE | H5PART_VFD_MPIPOSIX | H5PART_FS_LUSTRE, MPI_COMM_WORLD);
	
	file0 = H5PartOpenFileParallel(file0_name, H5PART_WRITE | H5PART_FS_LUSTRE, MPI_COMM_WORLD);
	file1 = H5PartOpenFileParallel(file1_name, H5PART_WRITE | H5PART_FS_LUSTRE, MPI_COMM_WORLD);
	file2 = H5PartOpenFileParallel(file2_name, H5PART_WRITE | H5PART_FS_LUSTRE, MPI_COMM_WORLD);
	file3 = H5PartOpenFileParallel(file3_name, H5PART_WRITE | H5PART_FS_LUSTRE, MPI_COMM_WORLD);
	file4 = H5PartOpenFileParallel(file4_name, H5PART_WRITE | H5PART_FS_LUSTRE, MPI_COMM_WORLD);
	file5 = H5PartOpenFileParallel(file5_name, H5PART_WRITE | H5PART_FS_LUSTRE, MPI_COMM_WORLD);

	if (rank == 0)
	{
		printf ("Opened H5Part file... \n");
	}
	// Throttle and see performance
	// H5PartSetThrottle (file, 10);

	H5PartWriteFileAttribString(file0, "Origin", "Tested by Matt");
	H5PartWriteFileAttribString(file1, "Origin", "Tested by Matt");
	H5PartWriteFileAttribString(file2, "Origin", "Tested by Matt");
	H5PartWriteFileAttribString(file3, "Origin", "Tested by Matt");
	H5PartWriteFileAttribString(file4, "Origin", "Tested by Matt");
	H5PartWriteFileAttribString(file5, "Origin", "Tested by Matt");

	MPI_Barrier (MPI_COMM_WORLD);
	timer_on (1);

	if (rank == 0) printf ("Before writing particles \n");
	create_and_write_synthetic_h5part_data(my_rank);

	MPI_Barrier (MPI_COMM_WORLD);
	timer_off (1);
	if (rank == 0) printf ("After writing particles \n");

	H5PartCloseFile(file0);
	H5PartCloseFile(file1);
	H5PartCloseFile(file2);
	H5PartCloseFile(file3);
	H5PartCloseFile(file4);
	H5PartCloseFile(file5);
	if (rank == 0) printf ("After closing particles \n");

	free(x); free(y); free(z);
	free(px); free(py); free(pz);
	free(id1);
	free(id2);

	MPI_Barrier (MPI_COMM_WORLD);

	timer_off (0);

	if (rank == 0)
	{
		printf ("\nTiming results\n");
		timer_msg (1, "just writing data");
		timer_msg (0, "opening, writing, closing file");
		printf ("\n");
	}

	MPI_Finalize();

	return 0;
}
