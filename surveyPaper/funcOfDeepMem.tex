\section{Functions of Deep Memories}
\label{sec:Functions of Deep Memories}
With the memory hierarchy being extended, especially with the addition of burst buffers, the role of the deep memory hierarchy in existing and new HPC functionality must be determined. Many of the functions that the extended memory hierarchy can serve are already offered by pre-exascale systems conceptualization. The addition of the new layers of memory offers more efficient ways of offering this same functionality while being able to offer new functionality. In this section, we explorer several functions of HPCs that have been changed or created by the advent of the deep memory hierarchy. 

\subsection{Scratch File System}
Many HPC applications write data to disk that is only used as a method from transitioning from one step in a work flow to another. This data is sometimes used for visualization or as an input data set for another application. The application wastes I/O time by writing the intermediate data to a much slower level of the now evolving memory hierarchy. Additionally, in the case of visualization, an additional penalty is brought about by the application needing to read the data from the hard drive based scratch file system and waiting for the slower storage to complete that task \cite{29}. For these reasons, using solid state based storage as the primary scratch file system is a simple means of accelerating many HPC applications. One of the first implemented features in Cray's Datawarp (which will be discussed in more detail later in this paper), is its ability to set up temporary job scratch space so that the data can reside on flash storage rather than the more sluggish parallel file system (PFS) \cite{8}.

\par
Another significant application for scratch systems comprised of solid state memory is faster checkpoint restart. Depending on the application and the amount of checkpoint data required, this data may need to be flushed to PFS, but in some cases it can just stay temporarily in scratch space. Applications spend a great deal of their run time on checkpoint operations, and utilizing storage that can significantly shorten the time for the checkpoint I/O phase will make the application more efficient overall and shorten run time \cite{21}. 

\subsection{Extended Memory}

Traditionally, when an operating system runs out of memory, it will allocate storage from the next level of the storage hierarchy \cite{34}. This has usually been hard disk based storage, which is many orders of magnitude slower than the main memory \cite{26}. This process occurs when HPC codes cannot store their working data set in memory \cite{22}. In the HPC community, this is called an out-of-core application. Because solid state disks are much faster, allocating dedicated solid state storage will offer performance benefits, compared to a shared parallel file system. Additionally, solid state storage is able to respond to random I/O events more efficiently than hard disk based storage, which allows for efficient non-sequential memory operations\cite{41}. This is critical, as memory access is largely non-sequential. An abstraction of the memory requests that are issued to the various levels of the extended memory heirarchy is included in Figure \ref{fig:OutOfCore}. Figure \ref{fig:OutOfCore} shows the relative number of memory requests to each layer in the hierarchy with the length representing time. Because most hot data (data that is commonly accessed) is stored in higher levels of memory, the number of requests to lower levels of memory are reduced through the additional layers \cite{10}. Utilizing solid state storage instead of disk based storage can greatly increase the efficiency of any out of core applications.

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{OutOfCore.png}
\caption{Extended Memory Hierarchy Resulting in Higher Utilization of Faster Memory Technologies} %maybe have one without? 
\label{fig:OutOfCore}
\end{figure}


%{File System Cache} 
%This seems to me the same as write/prefetch to me. For now, it is out of the paper 

%oldTODO determine how this is different from write/read operations/prefetch. Is it about adapative reach cache?, source 19 might clarify this, it has many use cases listed 

%source 21 talks more about a complete burst buffer system, similar to a file system cache s

%\subsection{Staging Area} %oldTODO determine if this section is worth keeping: how is this different from visualization and other workflows, aka simulation to simulation ?
 
\subsection{Prefetch} %read ops
Solid state drives have better read and write performance than their disk based counterparts. Many applications must read in large data sets, and could benefit from the accelerated read performance of a solid state based system \cite{20}. Since solid state disks are not yet cost effective for large scale, this data is stored on a hard disk based PFS \cite{1}. Jobs are scheduled on large scale computing, which makes a copy of the data to solid state based storage before the read operation begins, a simple prefetching problem. Unlike systems where the data is read in a quasi-random fashion, the data that must be fetched for a HPC application is known so an adaptive read cache does not need to be utilized. Solid state storage's exceptional sequential performance lends itself to having data loaded quickly from the PFS \cite{39}. Additionally, because solid state storage performs better than hard disk in both random and sequential operations, there would be no difference which read pattern the application uses, it would be accelerated \cite{41}.

%Note, source 5 shows that a PFS using lustre actually beats a datawarp system 
%Source 13 only mentions this, no details
%source 19 talks about read of checkpoints, but not with many references to burst buffers 
%source 21 talks more about a complete burst buffer system, similar to a file system cache
%source 24 didn't have much useful, may require a second read through  ?? 
%source 25 just mentions that IME has fast data reads as well, worth including?

\par 
Prefetching has experimentally shown a decrease in the application run time of several applications. One such application, TomoPy, was able to reduce full application runtime from approximately 310 seconds to approximately 210 seconds \cite{20}. Its read runtime was improved from approximately 42 seconds to about 5 seconds, making its read operation compete 8-9 times faster on SSD as compared to disk \cite{20}. The additional increase in performance results from continued access to disk after the initial read step \cite{42}. \fix{FIX this ref - how?} Additionally, the hard disk based systems showed high variability, with tests taking as much as 3 times the normal runtime \cite{20}. The solid state based system showed great consistency, with tests varying less than 2\% \cite{20}. 

%all about source 29, it does block caching. %TODO is this better for file system caching??

\par 
Because some data sets are too large to stage in to higher levels of memory, there is the potential to increase efficiency by utilizing block caching. Block caching can handle scenarios where the data set is too large for the limited storage space offered by solid state storage system. In a paper by Camp et al., a system is developed where blocks read from the PFS are then cached on the local solid state storage, resulting in a significant speed increased in subsequent read operations \cite{29}. Blocks are cached based on the frequency of access in an initial run \cite{29}. This is represented in Figure \ref{fig:blockCache}. Also localized block caching mechanisms is tested, and when compared with the solid state tests, the solid state drives performed two to eight times better \cite{29}. Some codes benefited more from this cached configuration of the memory hierarchy, such as the fusion code, which has 87\% of its blocks accelerated and 50\% faster \cite{29}. 


\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{BlockCaching.png}
\caption{Block Caching Operation Resulting in High Number of Cache Hits on Faster Solid State Storage}
\label{fig:blockCache}
\end{figure}

\subsection{Intermediate Storage Layers}
\label{sec:writeOperations}
HPC applications commonly generate large amounts of data that must be saved to storage subsystems. These storage subsystems are generally made up of hard disks. Data written to these systems takes three forms; checkpoint data that should persist after job completion, data generated for later analysis or visualization, and result data \cite{2} \cite{29}. This data is usually written in a phase based write pattern, where the application cycles between I/O and computation cycles \cite{1}. A sample of this I/O pattern is represented in Figure \ref{fig:writePattern}. Hard disk based storage systems are not able to handle this I/O pattern efficiently, and often reach maximum utilization during these I/O phases. The storage subsystems will sit idle during the compute phase of application, causing significant underutilization during the computational cycles of applications \cite{1}. 

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{writePattern.png}
\caption{Common I/O Bursty Pattern in HPC Applications}
\label{fig:writePattern}
\end{figure}

\par 

Additionally, many of these codes are structured to allow for all job threads to meet at a barrier after a job step. Because applications usually alternate between computation and I/O cycles, this means that all threads must wait at a barrier until the slowest thread has completed writing data. This phenomenon is known as blocking I/O, and is a major threat to computational efficiency. Cutting edge technologies such as Intel FastForward, which is discussed in depth later in this paper, implement non-blocking I/O techniques which utilize the extended memory hierarchy to solve this issue \cite{30}. A visual representation of blocking and non-blocking I/O is shown in Figures \ref{fig:blockingIO} and \ref{fig:nonBlockingIO}. As is clear in these figures, being able to continue without waiting on barriers for the slowest thread increases the overall efficiency of the application. 

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{BlockingIO.png}
\caption{Blocking I/O Due to MPI Barrier in HPC Applications, Resulting in Worse Performance}
\label{fig:blockingIO}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{NonBlockingIO.png}
\caption{Non-Blocking I/O with No MPI Barriers Resulting in Better Application Performance}
\label{fig:nonBlockingIO}
\end{figure}
 
\par

Utilizing solid state drives as an intermediate memory layer accelerates these I/O phases. This is because it is written to the high bandwidth solid state layer, which can be written to in a fraction of time compared to the disk-based storage subsystem. After the data has been written to persistent storage, in this case the SSD layer, the application can go back to its computation cycle \cite{1}. The acceleration brought about by this can be seen in Figure \ref{fig:blockingIO_SSD} as compared to Figure \ref{fig:blockingIO}. This data is then able to be transferred to the hard disk based PFS asynchronously, which allows the hard disk based system to be fully utilized for writing data during the computation cycle of the application \cite{1}. This allows the storage subsystems bandwidth to be used continuously and efficiently \cite{1}. The slow drain of data from the SSDs to the hard disk based system is important to allow data to continue to be written in stages to the SSD because of its relatively small size. This results in the application to take full advantage of the SSD's write throughput, as well as the capacity of the hard disk based PFS with relatively minimal overhead.

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{BlockingIO_SSD.png}
\caption{Accelerated I/O}
\label{fig:blockingIO_SSD}
\end{figure}

\subsection{Analysis \& Workflows}
Many HPC applications are multi-step, which results in another code reading the data of the first for visualization or other purposes. With the extended memory hierarchy, these subsequent steps, as well as the data writing, can be sped up considerably. Some configurations of this hierarchy involve the SSDs being placed on compute nodes so that as the data is written, another processing step can be started to allow a task such as a visualization to be completed sooner than it would in a traditional workflow \cite{16}. This is represented in Figure \ref{fig:workflows}. EMC and Los Alamos National Laboratory have tested one such workflow, which involved a fluid dynamics model and a wind turbine model \cite{16}.  With this configuration, the primary application completed 10\% more quickly, and the visualization, taking advantage of the co-processing vs post processing, finished in approximately four minutes after the primary application has completed compared to thirty minutes\cite{16}. 

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{Workflows.png}
\caption{The Use of Extended Memory in Accelerating Multi-Application Workflows}
\label{fig:workflows}
\end{figure}