\documentclass[conference]{IEEEtran}
\listfiles
\hyphenpenalty=2500
\tolerance=1000
\interfootnotelinepenalty=10000
\widowpenalty=10000
\clubpenalty=10000
%\usepackage{caption}
%\usepackage[font=footnotesize]{subfig}
\usepackage{subfig}
\usepackage[pdftex]{graphicx}
\usepackage[cmex10]{amsmath}
\usepackage{url}
\usepackage{color}
\usepackage{multirow}

%\setlength{\abovecaptionskip}{4pt}
%\setlength{\belowcaptionskip}{6pt}
%\setlength{\partopsep}{2pt}
%\setlength{\itemsep}{1pt}
%\setlength{\dblfloatsep}{1pt}
%\setlength{\dbltextfloatsep}{1pt}

%\setlength{\topsep}{2pt}
%\setlength{\floatsep}{2pt}

%\setlength{\textfloatsep}{5pt}
%\setlength{\intextsep}{5pt}

\providecommand{\e}[1]{\ensuremath{\times 10^{#1}}}
\DeclareMathOperator{\logit}{logit}

%\renewcommand{\comment}[1]{{\color{orange}~\\~\textsf{{#1}}}}
%\renewcommand{\comment}[1]{}
\definecolor{orange}{rgb}{1,0.5,0}
\newcommand \comment[1]{}			%  Silent version.
\renewcommand \comment[1]{{\color{red}~\\~\textbf{[#1]}}}
\newcommand{\fix}[1]{\textcolor{red}{\textbf{\textit{#1}}}}
\renewcommand\floatpagefraction{.9}
\renewcommand\dblfloatpagefraction{.9} % for two column documents
\renewcommand\topfraction{.9}
\renewcommand\dbltopfraction{.9} % for two column documents
\renewcommand\bottomfraction{.9}
\renewcommand\textfraction{.1}   
%\setcounter{totalnumber}{50}
%\setcounter{topnumber}{50}
%\setcounter{bottomnumber}{50}

%\setlength{\parskip}{1pt}
%\setlength{\parindent}{1pt}
%\def\IEEEbibitemsep{0pt plus .5pt}

%\makeatletter
%\def\@copyrightspace{\relax}
%\makeatother

\begin{document}

\title{
Improving VPIC I/O Performance Using Burst Buffers
}

\author{

\IEEEauthorblockN{Matt Bryson, Alex Sim, Suren Byna, John Wu\IEEEauthorrefmark{2}}
\IEEEauthorblockA{Lawrence Berkeley National Laboratory\\
National Energy Research Scientific Computing Center (NERSC)\IEEEauthorrefmark{2} \\
 Email: \{mbryson,asim,sbyna,kwu\IEEEauthorrefmark{2}\}@lbl.gov}
}

\maketitle
\begin{abstract}
%\input{abstract.tex}
With exascale computing on the horizon, high performance computing (HPC) systems will have I/O phases too large for modern parallel file systems to handle in reasonable time. These checkpoint restart phases often block computation, wasting computation time on the HPC. Current solutions to this problem include a tier of solid state backed storage systems, referred to as burst buffers, as well as optimization of the I/O software stack. In this work, we optimize the I/O phase of Vector Particle-in-Cell (VPIC) simulation code using burst buffer methodology. Our solution utilizes H5Part to implement stripe aligned write operations and to combine VPIC data into a single file. We extended the VPIC I/O kernel to use solid state drives caching in order to absorb I/O bursts. Compared to the hard drive based parallel file system on the Edison supercomputer, we demonstrated an improvement in the I/O phase completion time for VPIC in some instances.
\end{abstract}

\section{Introduction}
As high performance computing continues to increase in computational power, the discrepancy between computational and storage systems has become greater, leading to difficulties that have not been dealt with in the past. Until this point, storage systems have had to scale out to provide the amount of storage necessary for the high performance computers they support, and as a byproduct of this, have been able to support the I/O needs of these systems \cite{onTheRole}. As we begin to close the gap between petascale and exascale, this no longer becomes an economical solution \cite{onTheRole}. Additionally, entirely flash based systems are not economical for capacity \cite{bbFlashExascale}. With flash being efficient and economical in this regard, and hard disk storage being economical for storage capacity, it is common to use them together in a hybrid solution, to best take advantage of what each offers \cite{jitterFreeCoProcessing}. 

\par
It is common for the storage systems to use less than one third of their potential throughput 99\% of the time \cite{onTheRole}. This is due to the nature of high performance computer applications, which have generally have intermittent I/O patterns. One of these I/O patterns is the saving detailed simulation data for later analysis and allowing for the simulation to clear this more detailed data from the simulation \cite{vpicio}. This is necessary to free enough memory for the simulation to continue, but also results in large amounts of data being written to the parallel file system at once \cite{vpicio}. It is during these I/O phases that the storage system will be most utilized \cite{onTheRole}. Due to the large amount of I/O requests that are generated in a short period of time, this I/O pattern is referred to as "bursty" I/O \cite{onTheRole}. The second form of I/O that shares this "bursty" pattern is check pointing \cite{stagingIO}. Because simulations are ran across many nodes, and will continue to be ran across more, there is a high probability of failure on at least one of these nodes, causing the job to fail \cite{stagingIO}. To combat this, applications will write out checkpoints to persistent storage to allow the application to restart from checkpoint in the event of a failure \cite{stagingIO}. 

\par
These I/O phases waste computational time on modern supercomputers, and will waste more on next generation supercomputers, since all threads assigned to the job must wait until the I/O phase of the application is completed \cite{trillion_byna}. Waiting for a hard drive based parallel file system that is five orders of magnitude slower than the computational system it supports means that applications waste their run time completing I/O operations \cite{burstBufferRelatedWork}. Burst buffers, which are effectively I/O nodes equipped with SSDs and I/O forwarding software, set out to solve this problem \cite{onTheRole}. By allowing the application to write directly to burst buffers and having the I/O forwarding software asynchronously write to the PFS while the application continues computing, burst buffers allow for application I/O phases to complete in far less time than if they were written to the PFS directly.  


%It could be worthwhile to include issues with the software I/O stack here, if there is more to add in the paper later on. Likely, there will not be so it should not be included.
%check out paper a13 IOStackReform if this is to be included.
%\par
%In addition to these issues, the I/O stack for high performance computing systems is not %well coordinated, making the implementation of burst buffer features difficult. 

\section{Methodology}
In this work, we utilized the I/O kernel modeling the I/O phases of Vector Particle In Cell (VPIC), a plasma physics application generates an extremely large amount of data during it's I/O phases \cite{vpicio}. We take advantage of H5Part, a library designed to optimize the I/O from VPIC. H5Part accomplishes this by stripe aligning write operations from the VPIC I/O kernel by utilizing Cray MPI-IO \cite{vpicio}. Ensuring that stripe size is matched is important for achieving optimal performance on a Lustre based file system \cite{vpicio}. In addition to matching the stripe size, H5Part changes VPIC's normal I/O pattern, which involves creating an individual file per process to one larger file. This is necessary to achieve optimal performance at scale due to the large number of files that VPIC would create and the additional latency incurred by attempting to access these individual files \cite{vpicio}. Additionally, H5Part takes advantage of MPI-IO collective buffering, which reduces the number of processes sending I/O requests to the PFS, therefore reducing I/O contention \cite{vpicio}. These optimization techniques were useful in our testing of our middleware application, as burst buffer implementations commonly implement I/O optimization techniques because of their position between the HPC and PFS. 

\par
To best show a comparison between a traditional parallel file system, a burst buffer equipped system, and a system entirely comprised of SSD's we utilized NERSC's Alva. Alva is a Cray test bed equipped with six pairs of striped SSDs, which are accessible individually or as a group through Cray's DVS \cite{NERSC_alva}. Although configured to be striped, the configuration only allows one to be written to at a time, reducing the I/O rate to that of a single SSD. Prior benchmarks has put this around .9 GB/S. These grouped SSD's serve as the basis for the tests involving the solid state parallel file system, as well as the burst buffer implementation of the VPIC I/O kernel. We utilized a non production Lustre scratch space to serve as our parallel file system. Because of the control we had over the Lustre scratch space, we are able to present as close to a direct comparison as possible by adjusting the stripe size of the Lustre file system to match up with the amount of stripes we are able to achieve with the striped SSDs. For our final configuration were able to choose the optimal configuration of the Lustre system as well.

 %Additionally, we are able to match stripe sizes on each so the results demonstrate the difference in performance between the devices, and not storage system optimization. 

\par
Our work focused around developing a modified version of the VPIC-IO kernel to allow VPIC to utilize the SSDs on Alva as intermediate storage to reduce I/O phase time for the application. We approached this problem by implementing a queue structure and abstraction methods for H5Part as part of the I/O kernel. The I/O kernel sends write requests to the abstraction methods, which queue write requests with information relevant to the data's final location on the Lustre file system and the type of I/O operation that must later be removed from the queue. A single POSIX thread per job process runs in parallel with the application and can continually checks the queue structure for write requests. To ensure accurate results, we choose to wait until all write operations were completed before allowing the thread to begin. When this thread is run and checks that the queue has elements, it will remove requests from the queue. After removing this request from the queue, the thread uses the information from the queue to read the relevant HDF5 data space from the temporary file using H5Part on the SSD and write it to it's final location on Lustre.

\par
To show the difference between our implementation and the standard implementation of the VPIC I/O kernel, we ran tests with both the modified and unmodified kernel. Tests with the unmodified kernel were ran with various levels of striping on both solid state and hard drive based systems. These tests allowed us to choose a more optimal stripe count and stripe size for the Lustre file system. Due to the limitations of the solid state system, we choose only to include our tests that were set to stripe across a single solid state device. The modified kernel was tested both with a single stripe on both the solid state system and the lustre system, as well as with a single stripe on the solid state system and the optimal configuration of 8 object storage targets (OSTs) and a stripe size of 8 megabytes. 

\par 
Our configuration is several orders of magnitude smaller than current petascale systems, and even less representative of future exascale systems. We do not see this as an issue for our experimentation, as we the goal of this experiment is to show that supercomputer compute time can be used more effectively when burst buffers are introduced. We can only expect this trend to continue with test cases much larger than our own, as stripe count increases, so should the performance of the storage system. 
%include in discussions how this might not show scaling effects well?

\section{Results}
We tested three configurations of the Lustre file system to determine which would be best to test the modified I/O kernel with. It is important to note that we were limited to 8 OSTs. The three configurations were as follows: configuration one, which consisted of a single OST, configuration 2, which consisted of two OSTs with a stripe width of 8 megabytes, and configuration 3, which consisted of eight OSTs with a stripe width of 8 megabytes. The results of these tests, shown in figure 1 with multiple numbers of threads per test run, clearly demonstrate that out of the selected configurations configuration 3 is the optimal configuration.   

\begin{figure}[h]
\caption{}
\centering
\includegraphics[width=\linewidth]{lustreConfigs.png}
\end{figure}

\par
The single SSD configuration was also tested against Lustre configuration 1 to show the differences in performance. On the largest test, which was ran on 192 cores and generated 48 gigabytes of particle data Lustre configuration 1 had a best bandwidth of 0.37 gigabytes per second, and the single SSD had a best bandwidth of 0.77 gigabytes per second. There is a similar gap in bandwidth and I/O time, which is show in figure 2. 

\begin{figure}[h]
\caption{}
\centering
\includegraphics[width=\linewidth]{lustreSSD.png}
\end{figure}

\par 
Due to our limited choices of configuration for the solid state system, we choose to run the benchmark of our software with a single SSD. We also choose to test with the the most efficient Lustre configuration as to end up the lowest I/O rate possible. The chart below shows the time for our modified kernel to run at multiple thread counts. Because the solid state system is reliant on a single device, it's write and read times are both much longer than the Lustre write step. However, if these tests were run with a single Lustre OST we would see that the SSD write time is superior to the Lustre write time. Although the total time for this sequence of I/O to complete is longer than Lustre configuration 1 by itself, we must keep in mind that with an actual simulation, it could return to computation as soon as the SSD write phase is complete, and the asynchronous thread system will handle the SSD read step as well as the Lustre write step. 

\begin{figure}[h]
\caption{}
\centering
\includegraphics[width=\linewidth]{ssdC1lustreC3.png}
\end{figure}


%In our testing of our middleware application, runtime is significantly shorter for the primary thread generating I/O. Should the middleware application be completely separate from the job, the job would finish in approximately half the time for small runs, and (insert results from current experiments here). We conclude that burst buffers significantly shorten the run time of the primary application, which will allow for better HPC utilization, making burst buffer implementation an important task to ensure that future supercomputers fully utilize their computational abilities. 


\section{Discussion}
%what this shows
This work shows that utilizing burst buffers through flash storage and middleware software techniques, applications have the potential to be accelerated in their run time through shortened I/O steps. In this particular example, the VPIC I/O kernel just generates a final I/O phase, whereas in the actual simulation, there would be several I/O phases throughout the run time of the application. This means any savings from burst buffers would be increased, and during the compute phases of the application, the burst buffer software would be able to flush most of the data to it's final destination on the PFS, effectively mitigating the "bursty" nature of the I/O, and shortening the run time of the application overall. 

%setup limitations
\par
We must recognize the limitations in our setup to help guide future experimentation, as well as recognize any potential gaps in our data collection. Our primary limiting factor was scale, especially with our SSD configuration. It was our goal to show that an SSD array in the form of a custom burst buffer configuration was superior to the Lustre scratch space we utilized, but limitations in controlling the SSD array did not allow us to show this past a single OST. Additionally, we ran this on a limited scale, and our results do not show us what might occur with larger test runs and larger storage arrays, though we expect favorable results. 

%\section{Related Work}
 

\section{Conclusion}
With the growth of I/O phases as we approach exaflop level computing, modern disk based parallel file systems will no longer be able to serve as an economic and efficient solution without change. However, solid state disks are now effective from a cost perspective for performance, making pairing them with hard disk based systems an efficient and economic option. Utilizing solid state disks with I/O forwarding software allows the application to take advantage of the faster performance of the SSD and return to computation more quickly, shortening run time overall. Additionally, using SSDs to cache this "bursty" I/O allows for the parallel file system to be better utilized over time \cite{onTheRole}. 

\par
Utilizing our solid state system as a temporary storage location and using separate threads to write this data to it's final location on a Lustre parallel file system showed us burst buffers would be effective with the plasma physics code, VPIC. The length of it's I/O phase when ran on comparable configurations of the hard disk based Lustre system and the solid state system showed approximately a 53\% time savings. Utilizing burst buffer techniques for the VPIC code proves effective in shortening the I/O phases, and therefore the computation time overall, increased supercomputer computational efficiency. 

\section{Future Work}
%limitations of current work, reasoning for continuing to future work 
This work has more limited purpose, since it only shows improvement for a single scientific code. Additionally, it is being ran at a small scale, and does not adequately show some of the issues and advantages of being ran on a larger scale. The middleware implementation in this case is part of the I/O kernel itself, so we do not see the benefit of having this run on a server dedicated to I/O, as a true burst buffer implementation would. 

%include goals for future work here
\par
We plan to solve these issues with our future work. With Cori, NERSC's next generation supercomputer coming online soon, we will have a large scale system to test our burst buffer systems on. It will come equipped with a large number of I/O nodes equipped with SSDs, originally intended to be used with Cray's Datawarp middleware. Unfortunately, this software will not be fully released for an additional few years and will not be open source \cite{datawarp_doc}. This is one of primary the motivating factors for our future work. 

%plan for future work here 
\par
To accomplish these goals, we will be writing a separate middleware application that will work for many scientific codes. We are hopeful that we will be able to implement a solution that will be easy for the end user to integrate into their existing simulations. Our solution will be developed separately from any specific simulation code, and will run on separate nodes to avoid memory contention. Because of our accelerated timetable for this project, we will be able to optimize it and add features before Cray is able to finish with the launch of Datawarp. This will allow us to accelerate research in the field of burst buffers and supercomputer I/O, as well as improve efficiency for scientific computation in general.

% \vspace{3mm}
% \noindent
% {\textbf {\large Categories and Subject Descriptors:}} \\
% C.2.3 {\textbf{[Network Operations]}}: {Network monitoring;}
% C.4 {\textbf{[Performance of Systems]}}: {Modeling techniques;}
% G.3 {\textbf{[Probability and statistics]}}: {Time series Analysis}
% 
% 
% \vspace{1mm}
% \noindent
% {\bf General Terms:} Algorithms, Measurement, Performance
% 
% \vspace{1mm}
% \noindent
%\begin{IEEEkeywords}
%Performance Diagnosis, Machine Learning, %Scientific Cluster, Clustering, Dimensionality %Reduction
%\end{IEEEkeywords}
%\IEEEpeerreviewmaketitle

% Introduction
%\input{intro.tex}

% Related Work
%\input{related.tex}

% Implementation
%\input{impl.tex}  

% Experiments
%\input{exp.tex}

% Summary and Future work
%\input{con.tex}

%\fix {Acknowledgments should be there for all of our papers. This is the lab requirement.}
%ACKNOWLEDGMENTS are optional
\section{Acknowledgments}
I would like to thank my mentors Alex Sim and Suren Byna, as well as K. John Wu and Elizabeth Bautista. Additionally, I would like to thank the TRUST REU program, Aimee Tabor, and Tiffany Reardon, at the University of California Berkeley and Lawrence Berkeley National Laboratory. This work was supported by the National Science Foundation through grant CCF-0424422 and the Office of Advanced Scientific Computing Research, Office of Science, of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

\small
\bibliographystyle{IEEEtran}
\bibliography{BB_Tech}
\end{document}
