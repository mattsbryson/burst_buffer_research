#!/bin/sh

#PBS -q debug
#PBS -l mppwidth=24
#PBS -l walltime=00:01:00
#PBS -N mbryson_job


cd $PBS_O_WORKDIR 

module swap PrgEnv-cray PrgEnv-intel
aprun -n 24 ./bbtest
