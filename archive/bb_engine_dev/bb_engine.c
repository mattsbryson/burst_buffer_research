/* This is a burst buffer expiremental test for Suren's VPICIO simulation. The VPICIO simulation has been modified to point at this engine, which in turn points off to H5Part, which points to HDF5. This retains the performance increase brought about by H5Part 

Author: Matt Bryson <mbryson@lbl.gov> 
Coauthors: Suren Byna, Alex Sim - Code copied from previous projects, as this is an extention of VPICIO. 
Lawrence Berkeley National Lab, Berkeley CA 


Do not use queue calls directly in these methods, as there is the potential that threads will affect one another.. 
*/
//TODO write a thread handling class that handles the dequeing and method calling. These methods should still be allowed to queue.

#include <mpi.h>
#include "H5Part.h"
#include "H5Block.h"
#include <hdf5.h>
#include "queue.h"

#include <stdio.h>
#include <stdlib.h>

char* tempLoc = "/flash/scratch1/mbryson/bbTest/temp.h5";
H5PartFile* file;
H5PartFile *cFile;

//TODO double check if file should be closed in each method, or as a function of the thread handler when all threads are done.

//GENERAL NOTE write queue to write to disk from SSD, maintain catalog of what to write off. Return written to VCPI once the file is written to SSD, but leave in queue to go for disk. Use queue for different parts of the file 

void BBPartSetStep(h5part_int64_t step)
{
	
	char* file_name = tempLoc;
	
	H5PartSetStep(file, step);  
	
	Enqueue(path, "step", "step");
}

void BBQPartSetStep()
{
	//read from SSD file and get data out
	cFile = H5PartOpenFileParallel(tempLoc, H5PART_READ, MPI_COMM_WORLD);
	char* value;
	
	//wrong implmenetation, should just be 0 if the other is 0. Need more code to do multiple steps. 
	
	//TODO interposer code for multiple steps
	//code below is pretty much a hotfix
	h5part_int64_t step = 0;
		
	//= H5PartGetNumSteps(cFile);	
	
	
	char* file_name = path;
	
	
	H5PartSetStep(file, step);
	
	
	
	
	// this should not be handled by these methods, rather the threads when they are first intialized and given work. Dequeue();
}

void BBPartSetNumParticles(int nparticles)
{
	
	char* file_name = tempLoc;
	
	H5PartSetNumParticles(file, nparticles);
	
	
	Enqueue(path, "snp", "snp");
}

void BBQPartSetNumParticles(char* h5Name)
{
	//read from SSD file and get data out
	cFile = H5PartOpenFileParallel(tempLoc, H5PART_READ, MPI_COMM_WORLD);
	char* value;
	
	int nparticles = H5PartGetNumParticles(cFile);	
	
	
	char* file_name = path;
	
	
	H5PartSetNumParticles(file, nparticles);
	
	
	
	
	// this should not be handled by these methods, rather the threads when they are first intialized and given work. Dequeue();
}

void BBPartWriteDataFloat32(char* h5Name, float *x)
{
	
	char* file_name = tempLoc;
	


	H5PartWriteDataFloat32(file, h5Name, x);
	
	
	Enqueue(path, h5Name, "wdf32");
}

void BBQPartWriteDataFloat32(char* h5Name)
{
	//read from SSD file and get data out
	cFile = H5PartOpenFileParallel(tempLoc, H5PART_READ, MPI_COMM_WORLD);
	char* value;
	
	
	int nparticles = H5PartGetNumParticles(cFile);
	//float *x = (double*)malloc(nparticles*sizeof(double));
	float *x = (float*)malloc(nparticles*sizeof(float)); //Looks like it worked! Check this if there are runtime errors. 
	H5PartReadDataFloat32(cFile, h5Name, x);		
	
	
	char* file_name = path;
	
	
	H5PartWriteDataFloat32(file, h5Name, x);
	
	
	
	
	// this should not be handled by these methods, rather the threads when they are first intialized and given work. Dequeue();
}
	 
void BBPartWriteDataInt32(char* h5Name, h5part_int32_t *id)
{

	
	char* file_name = tempLoc;
	


	H5PartWriteDataInt32(file, h5Name, id);
	
	
	Enqueue(path, h5Name, "wdi32");
}

void BBQPartWriteDataInt32(char* h5Name)
{
	//read from SSD file and get data out
	cFile = H5PartOpenFileParallel(tempLoc, H5PART_READ, MPI_COMM_WORLD);
	char* value;
	
	
	int nparticles = H5PartGetNumParticles(cFile);
	int *id = (int*)malloc(nparticles*sizeof(int));
	H5PartReadDataInt32(cFile, h5Name, id);		
	
	
	char* file_name = path;
	
	
	H5PartWriteDataInt32(file, h5Name, id);
	
	
	
	
	// this should not be handled by these methods, rather the threads when they are first intialized and given work. Dequeue();
}


//TODO replace this in vpicio with path declaration, files should not be handled directly from there. 


//this is an interposer for H5PartWriteFileAttribString. Don't forget path! It's a new thing for this lib. Path will always go where files used to.
void BBPartWriteFileAttribString(char* a, char* b) 
{
	
	char* file_name = tempLoc;
	
	//write to ssd using normal method
	//queue operation to HD system
	//complete, make sure some engine is emptying the queue out. 
	H5PartWriteFileAttribString(file, a, b);
	
	
	Enqueue(path, a, "wfas");
}

//q stands for queue, this is the companion function to clear stuff off the queue for the function above. 
void BBQPartWriteFileAttribString(char* h5Name)
{
	//read from SSD file and get data out
	cFile = H5PartOpenFileParallel(tempLoc, H5PART_READ, MPI_COMM_WORLD);
	char* value;
	
	H5PartReadFileAttrib(cFile, h5Name, value);	
	
	
	char* file_name = path;
	
	
	H5PartWriteFileAttribString(file, h5Name, value);
	
	
	
	
	// this should not be handled by these methods, rather the threads when they are first intialized and given work. Dequeue();
}

void BBPartOpenFileParallel ()
{
	file = H5PartOpenFileParallel (tempLoc, H5PART_WRITE, MPI_COMM_WORLD);
}

void BBQPartOpenFileParallel (char* path)
{
	cFile = H5PartOpenFileParallel (file_name, H5PART_WRITE, MPI_COMM_WORLD);
}

void BBPartCloseFile()
{
	H5PartCloseFile(file);
}

void BBQPartCloseFile()
{
	H5PartCloseFile(cFile);
}

//TODO CLOSE FILE Remove this from VPICIO, it should not handle files directly 

