#include <stdlib.h>                                                               
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/stat.h>
#include <mpi.h>

//should be ran on alva mom node

int main (int argc, char* argv[]) {
	int myid, nprocs, length;  // MPI info
	char name[512];  //node name
	int ind_start=0,ind_end=0; // jobs are evenly allocated to each processor
    double t_start,t_end; // mpi timer
    double t_start2,t_end2; // mpi timer
	FILE *fptr;
	char dirname[128];
	char filename[128];
	sprintf(dirname, "/flash/scratch1/mbryson");
	//sprintf(dirname, "./mbryson");
    mkdir(dirname, S_IRWXU|S_IRGRP|S_IXGRP);

    /* Initialize MPI */
    MPI_Init(&argc, &argv);
    /*get process id, total numbers and node name*/
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Get_processor_name(name,&length);

    if(myid == 0){
	    printf("**********************************************************\n");
	    printf("Matt BBtest: process %d of %d on %s\n", myid, nprocs, name);
	    printf("**********************************************************\n");
    }

	sprintf(filename, "%s/test_%d", dirname, myid);
	if ((fptr = fopen(filename,"w+"))==NULL){
        printf("Error: Cannot Open File %s\n", filename);
    }

	/*record the starting time for each process*/
    MPI_Barrier(MPI_COMM_WORLD); 
    t_start = MPI_Wtime();

	printf("Hi BBtest from process %d of %d on %s\n", myid, nprocs, name);
	fprintf(fptr, "Hi BBtest from process %d of %d on %s\n", myid, nprocs, name);

	/*record the ending time for each process*/
	MPI_Barrier(MPI_COMM_WORLD); 
    t_end = MPI_Wtime();
	fclose(fptr);

	printf("PID=%d  time= %f \n", myid, t_end-t_start);

    /* closing MPI */
	MPI_Finalize();

	return 0;
}
