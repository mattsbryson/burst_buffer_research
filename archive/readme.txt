ssh -AY edison.nersc.gov
module load PrgEnv-intel

ssh -AY alva.nersc.gov

qsub bbtest_qsub.sh
qs -u asim
qstat -u asim
showq

qsub bbtest_clean_qsub.sh

xtnodestat
qstat -Qf

use "regular" or "debug" queue
use "bench"

module use /project/projectdirs/m1248/asim_data/bb/modules
module load hdf5/1.8.14 h5part

