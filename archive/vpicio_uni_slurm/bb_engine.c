/* This is a burst buffer expiremental test for Suren's VPICIO simulation. The VPICIO simulation has been modified to point at this engine, which in turn points off to H5Part, which points to HDF5. This retains the performance increase brought about by H5Part 

Author: Matt Bryson <mbryson@lbl.gov> 
Coauthors: Suren Byna, Alex Sim - Code copied from previous projects, as this is an extention of VPICIO. 
Lawrence Berkeley National Lab, Berkeley CA 


Do not use queue calls directly in these methods, as there is the potential that threads will affect one another.. 
*/
//TODO write a thread handling class that handles the dequeing and method calling. These methods should still be allowed to queue.

#include <mpi.h>
#include "H5Part.h"
#include "H5Block.h"
#include <hdf5.h>
#include "queue.h"
#include "timer.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern int my_rank;
extern float totalRead;
extern float totalWrite;
extern float totalMetadata;
extern float totalWait;

extern char* path;
extern char* tempLoc;

//Warn will turn on warnings from the middleware 
//#define WARN


//The following commented code was created to 
//int nprocs;
//MPI_Comm_size(MPI_COMM_WOLRD, &nprocs);
/*
char* fName = "/flash/scratch1/mbryson/temp";
char* fType = ".h5";
char* tempLoc = fName + nprocs + fType;
*/
//char* tempLoc = "/dwphase1/mbryson/temp.h5";
//char* path = "/dwphase1/mbryson/temp";
H5PartFile* tempFile;
//H5PartFile* tempFileRead;
H5PartFile* file;

//TODO double check if file should be closed in each method, or as a function of the thread handler when all threads are done.

//GENERAL NOTE write queue to write to disk from SSD, maintain catalog of what to write off. Return written to VCPI once the file is written to SSD, but leave in queue to go for disk. Use queue for different parts of the file 

void BBPartSetStep(char* path, h5part_int64_t step)
{	
	H5PartSetStep(tempFile, step);  
	Enqueue(path, "step", "step");
}

void BBQPartSetStep()
{
	//creates new timer 
	int t = 5;
	if(my_rank == 0)
	{
		timer_on(t);
	}
		
	//TODO interposer code for multiple steps
	//code below is pretty much a hotfix
	//full fix could be easily accomplished by queuing more info, or not storing all data in the H5Part. 
	h5part_int64_t step = 0;	
	//= H5PartGetNumSteps(tempFile);	
	H5PartSetStep(file, step);	
	
	timer_off(t);
	if(my_rank == 0)
	{
		//timer_msg(t, "Reading a float dataspace");
		totalMetadata = totalMetadata + timer_get(t);
	}
	timer_reset(t);
}

void BBPartSetNumParticles(char* path, int nparticles)
{
	H5PartSetNumParticles(tempFile, nparticles);	
	Enqueue(path, "snp", "snp");
}

void BBQPartSetNumParticles(char* path, char* h5Name)
{
	int t = 6;
	if(my_rank == 0)
	{
		timer_on(t);
	}
	
	int nparticles = H5PartGetNumParticles(tempFile);	
	H5PartSetNumParticles(file, nparticles);
	
	timer_off(t);
	if(my_rank == 0)
	{
		//timer_msg(t, "Reading a float dataspace");
		totalMetadata = totalMetadata + timer_get(t);
	}
	timer_reset(t);
}

void BBPartWriteDataFloat32(char* path, char* h5Name, float *x)
{
	H5PartWriteDataFloat32(tempFile, h5Name, x);
	Enqueue(path, h5Name, "wdf32");
}

void BBQPartWriteDataFloat32(char* path, char* h5Name)
{
	int t = 3;
	if(my_rank == 0)
	{
		timer_on(t);
	}

	int nparticles = H5PartGetNumParticles(tempFile);
	float *x = (float*)malloc(nparticles*sizeof(double));
	
	H5PartReadDataFloat32(tempFile, h5Name, x);		

	timer_off(t);
	if(my_rank == 0)
	{
		timer_msg(t, "Reading a float dataspace");
		totalRead = totalRead + timer_get(t);
	}
	timer_reset(t);
	
	if(my_rank == 0)
	{
		timer_on(t);
	}
	
	H5PartWriteDataFloat32(file, h5Name, x);
	
	timer_off(t);
	if(my_rank == 0)
	{
		//timer_msg(t, "Reading a float dataspace");
		totalWrite = totalWrite + timer_get(t);
	}
	timer_reset(t);
	
	free(x);
}
	 
void BBPartWriteDataInt32(char* path, char* h5Name, h5part_int32_t *id)
{
	H5PartWriteDataInt32(tempFile, h5Name, id);
	Enqueue(path, h5Name, "wdi32");
}

void BBQPartWriteDataInt32(char* path, char* h5Name)
{
	//read from SSD file and get data out
	
	int t = 4;
	if(my_rank == 0)
	{
		timer_on(t);
	}
	
	int nparticles = H5PartGetNumParticles(tempFile);
	h5part_int32_t *id = (h5part_int32_t*)malloc(nparticles*sizeof(h5part_int32_t));
	H5PartReadDataInt32(tempFile, h5Name, id);		
	
	
	timer_off(t);
	if(my_rank == 0)
	{
		timer_msg(t, "Reading a int dataspace");
		totalRead = totalRead + timer_get(t);
	}
	timer_reset(t);
	
	if(my_rank == 0)
	{
		timer_on(t);
	}
	
	H5PartWriteDataInt32(file, h5Name, id);
	
	timer_off(t);
	if(my_rank == 0)
	{
		//timer_msg(t, "Reading a float dataspace");
		totalWrite = totalWrite + timer_get(t);
	}
	timer_reset(t);
	
	free(id);
}

void BBPartWriteFileAttribString(char* path, char* h5Name, char* attrib) 
{
	//write to ssd using normal method
	//queue operation to HD system
	//complete, make sure some engine is emptying the queue out. 
	int error = H5PartWriteFileAttribString(tempFile, h5Name, attrib);	
	if(error == 0)
	{
#ifdef WARN
		printf("Failed to write attrib");
#endif
	}
	Enqueue(path, h5Name, "wfas");
}

//q stands for queue, this is the companion function to clear stuff off the queue for the function above. 
void BBQPartWriteFileAttribString(char* path, char* h5Name)
{
	int t = 7;
	if(my_rank == 0)
	{
		timer_on(t);
	}
	
	//read from SSD file and get data out
	char* value = malloc(1024*sizeof(char));
	//char* value;
	int error;

	error = H5PartReadFileAttrib(tempFile, h5Name, value);
	if(error == 0)
	{
#ifdef WARN
		printf("Failed to read attrib.");
#endif
	}
	else
	{
		H5PartWriteFileAttribString(file, h5Name, value);
	}
	
	timer_off(t);
	if(my_rank == 0)
	{
		//timer_msg(t, "Reading a float dataspace");
		totalMetadata = totalMetadata + timer_get(t);
	}
	timer_reset(t);
}

void BBPartOpenFileParallel (char* path)
{	
	tempFile = H5PartOpenFileParallel (tempLoc, H5PART_WRITE | H5PART_FS_LUSTRE, MPI_COMM_WORLD);
	
	Enqueue(path, "open", "open");
}

void BBPartOpenFile (char* path, int rank)
{	
	char* eof = ".h5";
	char* buffer;
	sprintf(buffer, "%s%d%s", path, rank, eof);
	
	//printf(buffer);
	
	tempFile = H5PartOpenFile (buffer, H5PART_WRITE | H5PART_FS_LUSTRE);
	
	Enqueue(path, "open", "open");
}

void BBQPartOpenFileParallel (char* path)
{
	int t = 8;
	if(my_rank == 0)
	{
		timer_on(t);
	}
	
	//H5PartCloseFile(tempFile);
	//tempFileRead = H5PartOpenFileParallel (tempLoc, H5PART_READ | H5PART_FS_LUSTRE, MPI_COMM_WORLD);
	file = H5PartOpenFileParallel (path, H5PART_WRITE | H5PART_FS_LUSTRE, MPI_COMM_WORLD);
	
	timer_off(t);
	if(my_rank == 0)
	{
		//timer_msg(t, "Reading a float dataspace");
		totalMetadata = totalMetadata + timer_get(t);
	}
	timer_reset(t);
}

void BBQPartOpenFile (char* path, int rank)
{	
	char* eof = ".h5";
	char* buffer;
	sprintf(buffer, "%s%d%s", path, rank, eof);
	file = H5PartOpenFileParallel (buffer, H5PART_WRITE | H5PART_FS_LUSTRE, MPI_COMM_WORLD);
	
	Enqueue(path, "open", "open");
}

void BBPartCloseFile()
{
	Enqueue(NULL, "close", "close");
}

void BBQPartCloseFile()
{
	int t = 9;
	if(my_rank == 0)
	{
		timer_on(t);
	}
	
	//Moved for testing purposes to the bbq method that opens the real file 
	H5PartCloseFile(tempFile);
	H5PartCloseFile(file);
	
	timer_off(t);
	if(my_rank == 0)
	{
		//timer_msg(t, "Reading a float dataspace");
		totalMetadata = totalMetadata + timer_get(t);
	}
	timer_reset(t);
}


