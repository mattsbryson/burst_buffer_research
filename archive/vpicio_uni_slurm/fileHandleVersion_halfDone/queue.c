//Code used from: https://gist.github.com/mycodeschool/7510222
//Modified by Matt Bryson <mbryson@lbl.gov>

#include<stdio.h>
#include<stdlib.h>
#include "H5Part.h"
#include "H5Block.h"
#include <hdf5.h>

struct Node {
	H5PartFile *tempFile;
	H5PartFile *file;
	//char* path;
	char* h5Name;
	char* request; //should be formatted as SS for set step, SNP for set num particles, etc. 
	struct Node* next;
};
// Two glboal variables to store address of front and rear nodes. 
struct Node* front = NULL;
struct Node* rear = NULL;
 
// To Enqueue an integer
void Enqueue(char* p, char* name, char* req) {
	struct Node* temp = 
		(struct Node*)malloc(sizeof(struct Node));
	temp->path = p; 
	temp->h5Name = name;
	temp->request = req;
	temp->next = NULL;
	if(front == NULL && rear == NULL){
		front = rear = temp;
		#pragma warning disable 117
		return;
	}
	rear->next = temp;
	rear = temp;
}
 
// To Dequeue a struct.
void Dequeue() {
	struct Node* temp = front;
	if(front == NULL) {
		printf("Queue is Empty\n");
		#pragma warning disable 117
		return;
	}
	if(front == rear) {
		front = rear = NULL;
	}
	else {
		front = front->next;
	}
	free(temp);
}
/*
char* pathFront() {
	if(front == NULL) {
		printf("Queue is empty\n");
		return "empty";
	}
	return front->path;
}
*/

H5PartFile* tempFileFront() {
	if(front == NULL) {
		printf("Queue is empty\n");
		return NULL;
	}
	return front->tempFile;
}

H5PartFile* fileFront() {
	if(front == NULL) {
		printf("Queue is empty\n");
		return NULL;
	}
	return front->file;
}

char* h5NameFront() {
	if(front == NULL) {
		printf("Queue is empty\n");
		return "empty";
	}
	return front->h5Name;
}

char* reqFront() {
	if(front == NULL) {
		printf("Queue is empty\n");
		return "empty";
	}
	return front->request;
}
 
void Print() {
	struct Node* temp = front;
	while(temp != NULL) {
		printf("%s ",temp->h5Name);
		temp = temp->next;
	}
	printf("\n");
}