/* This is a burst buffer expiremental test for Suren's VPICIO simulation. The VPICIO simulation has been modified to point at this engine, which in turn points off to H5Part, which points to HDF5. This retains the performance increase brought about by H5Part 

Author: Matt Bryson <mbryson@lbl.gov> 
Coauthors: Suren Byna, Alex Sim - Code copied from previous projects, as this is an extention of VPICIO. 
Lawrence Berkeley National Lab, Berkeley CA 


Do not use queue calls directly in these methods, as there is the potential that threads will affect one another.. 
*/
//TODO write a thread handling class that handles the dequeing and method calling. These methods should still be allowed to queue.

#include <mpi.h>
#include "H5Part.h"
#include "H5Block.h"
#include <hdf5.h>
#include "queue.h"

#include <stdio.h>
#include <stdlib.h>

char* tempLoc = "/flash/scratch1/mbryson/bbTest/temp.h5";

//TODO double check if file should be closed in each method, or as a function of the thread handler when all threads are done.

//GENERAL NOTE write queue to write to disk from SSD, maintain catalog of what to write off. Return written to VCPI once the file is written to SSD, but leave in queue to go for disk. Use queue for different parts of the file 

void BBPartSetStep(H5PartFile *tempFile, H5PartFile *file, h5part_int64_t step)
{	
	H5PartSetStep(tempFile, step);  
	Enqueue(tempFile, file, "step", "step");
}

void BBQPartSetStep(H5PartFile *tempFile, H5PartFile *file)
{
	//read from SSD file and get data out
	char* value;
	//wrong implmenetation, should just be 0 if the other is 0. Need more code to do multiple steps. 	
	//TODO interposer code for multiple steps
	//code below is pretty much a hotfix
	//full fix could be easily accomplished by queuing more info, or not storing all data in the H5Part. 
	h5part_int64_t step = 0;	
	//= H5PartGetNumSteps(tempFile);	
	H5PartSetStep(file, step);	
}

void BBPartSetNumParticles(H5PartFile *tempFile, H5PartFile *file, int nparticles)
{
	H5PartSetNumParticles(tempFile, nparticles);	
	Enqueue(tempFile, file, "snp", "snp");
}

void BBQPartSetNumParticles(H5PartFile *tempFile, H5PartFile *file, char* h5Name)
{
	//read from SSD file and get data out
	char* value;	
	int nparticles = H5PartGetNumParticles(tempFile);	
	//TODO get rid of all of the lines below, they aren't needed anymore
	
	H5PartSetNumParticles(file, nparticles);
}

void BBPartWriteDataFloat32(H5PartFile *tempFile, H5PartFile *file, char* h5Name, float *x)
{
	H5PartWriteDataFloat32(tempFile, h5Name, x);
	Enqueue(tempFile, file, h5Name, "wdf32");
}

void BBQPartWriteDataFloat32(H5PartFile *tempFile, H5PartFile *file, char* h5Name)
{
	//read from SSD file and get data out
	//TODO is value needed?
	char* value;
	int nparticles = H5PartGetNumParticles(tempFile);
	//float *x = (double*)malloc(nparticles*sizeof(double));
	float *x = (float*)malloc(nparticles*sizeof(float)); //Looks like it worked! Check this if there are runtime errors. 
	H5PartReadDataFloat32(tempFile, h5Name, x);		
	
	H5PartWriteDataFloat32(file, h5Name, x);
}
	 
void BBPartWriteDataInt32(H5PartFile *tempFile, H5PartFile *file, char* h5Name, h5part_int32_t *id)
{
	H5PartWriteDataInt32(tempFile, h5Name, id);
	Enqueue(tempFile, file, h5Name, "wdi32");
}

void BBQPartWriteDataInt32(H5PartFile *tempFile, H5PartFile *file, char* h5Name)
{
	//read from SSD file and get data out
	char* value;
	
	int nparticles = H5PartGetNumParticles(tempFile);
	int *id = (int*)malloc(nparticles*sizeof(int));
	H5PartReadDataInt32(tempFile, h5Name, id);		
	H5PartWriteDataInt32(file, h5Name, id);
}

void BBPartWriteFileAttribString(H5PartFile *tempFile, H5PartFile *file, char* h5Name, char* attrib) 
{
	//write to ssd using normal method
	//queue operation to HD system
	//complete, make sure some engine is emptying the queue out. 
	H5PartWriteFileAttribString(tempFile, h5Name, attrib);	
	Enqueue(tempFile, file, h5Name, "wfas");
}

//q stands for queue, this is the companion function to clear stuff off the queue for the function above. 
void BBQPartWriteFileAttribString(H5PartFile *tempFile, H5PartFile *file, char* h5Name)
{
	//read from SSD file and get data out
	char* value;
	
	H5PartReadFileAttrib(tempFile, h5Name, value);	
	H5PartWriteFileAttribString(file, h5Name, value);
}

H5PartFile BBPartOpenFileParallel (char* path)
{	
	H5PartFile* tempFile;
	tempFile = H5PartOpenFileParallel (tempLoc, H5PART_WRITE, MPI_COMM_WORLD);
	
	Enqueue(tempFile, file, "open", "open");
	
	return tempFile;
}

H5PartFile BBQPartOpenFileParallel (char* path)
{
	H5PartFile* file;
	file = H5PartOpenFileParallel (path, H5PART_WRITE, MPI_COMM_WORLD);
	return file;
}

void BBPartCloseFile(H5PartFile *tempFile, H5PartFile *file)
{
	Enqueue(tempFile, file, "close", "close");
}

void BBQPartCloseFile(H5PartFile *tempFile, H5PartFile *file)
{
	H5PartCloseFile(tempFile);
	H5PartCloseFile(file);
}


