// Description: This is a simple benchmark based on VPIC's I/O interface
//		Each process writes a specified number of particles into 
//		a h5part output file
// Author:	Suren Byna <SByna@lbl.gov>
//		Lawrence Berkeley National Laboratory, Berkeley, CA
// 


#include <mpi.h>

#include "H5Part.h"
#include "H5Block.h"

#include "queue.h"
#include "bb_engine.h"

#include <hdf5.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


// A simple timer based on gettimeofday
#include "./timer.h"
struct timeval start_time[3];
float elapse[3];

int numparticles = 8388608;	// 8  meg particles per process
float *x, *y, *z;
float *px, *py, *pz;
h5part_int32_t *id1, *id2;
int x_dim = 64;
int y_dim = 64; 
int z_dim = 64;

H5PartFile* file;
H5PartFile* tempFile;

// Uniform random number
inline double uniform_random_number() 
{
	return (((double)rand())/((double)(RAND_MAX)));
}

// Initialize particle data
void init_particles ()
{
	int i;
	for (i=0; i<numparticles; i++) 
	{
		id1[i] = i;
		id2[i] = i*2;
		x[i] = uniform_random_number()*x_dim;
		y[i] = uniform_random_number()*y_dim;
		z[i] = ((double)id1[i]/numparticles)*z_dim;    
		px[i] = uniform_random_number()*x_dim;
		py[i] = uniform_random_number()*y_dim;
		pz[i] = ((double)id2[i]/numparticles)*z_dim;    
	}
}

// Create BBPart file and write data
void create_and_write_synthetic_h5part_data(int rank) 
{
	
	
	// Note: printf statements are inserted basically 
	// to check the progress. Other than that they can be removed
	BBPartSetStep(file, 0); // only 1 timestep in this file
	if (rank == 0) printf ("Step is set \n");
	BBPartSetNumParticles(file, numparticles);
	if (rank == 0) printf ("Num particles is set \n");

	BBPartWriteDataFloat32(file,"x",x); 
	if (rank == 0) printf ("Written variable 1 \n");
	BBPartWriteDataFloat32(file,"y",y);
	if (rank == 0) printf ("Written variable 2 \n");
	BBPartWriteDataFloat32(file,"z",z);
	if (rank == 0) printf ("Written variable 3 \n");

	BBPartWriteDataInt32(file,"id1",id1);
	if (rank == 0) printf ("Written variable 4 \n");
	BBPartWriteDataInt32(file,"id2",id2);
	if (rank == 0) printf ("Written variable 5 \n");

	BBPartWriteDataFloat32(file,"px",px); 
	if (rank == 0) printf ("Written variable 6 \n");
	BBPartWriteDataFloat32(file,"py",py);
	if (rank == 0) printf ("Written variable 7 \n");
	BBPartWriteDataFloat32(file,"pz",pz);
	if (rank == 0) printf ("Written variable 8 \n");
}

int main (int argc, char* argv[]) 
{
	char *file_name = argv[1];
	
	
	//printf("Path: ");
	//printf(argv[1]);
	
	MPI_Init(&argc,&argv);
	int my_rank, num_procs;
	MPI_Comm_rank (MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size (MPI_COMM_WORLD, &num_procs);

	if (argc == 3)
	{
		numparticles = (atoi (argv[2]))*1024*1024;
	}
	else
	{
		numparticles = 8*1024*1024;
	}

	if (my_rank == 0) {printf ("Number of paritcles: %ld \n", numparticles);}

	x=(float*)malloc(numparticles*sizeof(double));
	y=(float*)malloc(numparticles*sizeof(double));
	z=(float*)malloc(numparticles*sizeof(double));

	px=(float*)malloc(numparticles*sizeof(double));
	py=(float*)malloc(numparticles*sizeof(double));
	pz=(float*)malloc(numparticles*sizeof(double));

	id1=(h5part_int32_t*)malloc(numparticles*sizeof(h5part_int32_t));
	id2=(h5part_int32_t*)malloc(numparticles*sizeof(h5part_int32_t));

	init_particles ();

	if (my_rank == 0)
	{
		printf ("Finished initializing particles \n");
	}

	MPI_Barrier (MPI_COMM_WORLD);
	timer_on (0);
	//file = BBPartOpenFileParallelAlign(file_name, BBPART_WRITE, MPI_COMM_WORLD, alignf);
	// file = BBPartOpenFileParallel (file_name, BBPART_WRITE | BBPART_VFD_MPIPOSIX | BBPART_FS_LUSTRE, MPI_COMM_WORLD);
	//file = BBPartOpenFileParallel (file_name, BBPART_WRITE | BBPART_FS_LUSTRE, MPI_COMM_WORLD);
	//removed, handled by bb_engine now file = BBPartOpenFileParallel (file_name, BBPART_WRITE, MPI_COMM_WORLD);
	
	BBPartOpenFileParallel (file_name);

	if (my_rank == 0)
	{
		printf ("Opened BBPart file... \n");
	}
	// Throttle and see performance
	// BBPartSetThrottle (file, 10);

	BBPartWriteFileAttribString(file, "Origin", "Tested by Matt");

	MPI_Barrier (MPI_COMM_WORLD);
	timer_on (1);

	if (my_rank == 0) printf ("Before writing particles \n");
	create_and_write_synthetic_h5part_data(my_rank);

	MPI_Barrier (MPI_COMM_WORLD);
	timer_off (1);
	if (my_rank == 0) printf ("After writing particles \n");

	//should not need because BB_Engine BBPartCloseFile(file);
	
	//TODO double check location of file close 
	BBPartCloseFile();
	
	if (my_rank == 0) printf ("After closing particles \n");

	free(x); free(y); free(z);
	free(px); free(py); free(pz);
	free(id1);
	free(id2);

	MPI_Barrier (MPI_COMM_WORLD);

	timer_off (0);
	
	if (my_rank == 0)
	{
		printf ("\nTiming results\n");
		timer_msg (1, "just writing data");
		timer_msg (0, "opening, writing, closing file");
		printf ("\n");
	}
	
	
	
	//quick test to see that it can copy the file using the queue struct 
	//this code will empty out the queue, and write out to the file. It acts at the central engine for the code, choosing what to call. 
	
	//files need to be opened first using file open commands - part of BB_ENGINE 
	char* req = reqFront();
	char* done = "empty";
	while (strcmp(req,done) != 0)
	{
		
		printf(req);
		printf(": ");
		
		
		if(strcmp(req, "step") == 0)
		{
			//printf(h5NameFront());
			BBQPartSetStep(file);
		}
		else if(strcmp(req, "snp") == 0)
		{
			//printf(h5NameFront());
			BBQPartSetNumParticles(file, h5NameFront());
		}
		else if(strcmp(req, "wdf32") == 0)
		{
			//printf(h5NameFront());
			BBQPartWriteDataFloat32(file, h5NameFront());
		}
		else if(strcmp(req, "wdi32") == 0)
		{
			//printf(h5NameFront());
			BBQPartWriteDataInt32(file, h5NameFront());
		}
		else if(strcmp(req, "wfas") == 0)
		{
			//printf(h5NameFront());
			BBQPartWriteFileAttribString(file, h5NameFront());
		}
		else if(strcmp(req, "open") == 0)
		{
			BBQPartOpenFileParallel(file);
		}
		else if(strcmp(req, "close") == 0)
		{
			BBQPartCloseFile();
		}
		else
		{
			printf("Note a valid request\n");
		}
		
		
		printf("\n");
		
		Dequeue();
		req = reqFront();
	}
	


	MPI_Finalize();

	return 0;
}
