#PBS -q bench
#PBS -l mppwidth=24
#PBS -l walltime=00:30:00
#PBS -N asim_test_job
#PBS -j oe 
 
setenv CRAY_ROOTFS DSL 
setenv TMP_LOGDATE "`date '+%y%m%d%H%M%S'`"

cd $PBS_O_WORKDIR 
aprun -n 2 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch1/asim/test1.h5 2000 >& log_test1_$TMP_LOGDATE.log

