/* This file was automatically generated.  Do not edit! */
#ifndef queue_h
#define queue_h

void Print();
char *reqFront();
char *h5NameFront();
char *pathFront();
int qSize(); 
void Dequeue();
void Enqueue(char *p,char *name,char *req);
extern struct Node *rear;
extern struct Node *front;

#endif