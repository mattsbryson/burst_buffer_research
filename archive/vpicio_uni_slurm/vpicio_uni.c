// Description: This is a simple benchmark based on VPIC's I/O interface
//		Each process writes a specified number of particles into 
//		a h5part output file
// Author:	Suren Byna <SByna@lbl.gov>
//		Lawrence Berkeley National Laboratory, Berkeley, CA
// 


#include <mpi.h>

#include "H5Part.h"
#include "H5Block.h"

#include "queue.h"
#include "bb_engine.h"

#include <hdf5.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <pthread.h>
#include <unistd.h>

typedef int bool;
#define true 1
#define false 0


#define par //this controls if it will generate many small files, or a large file

// A simple timer based on gettimeofday
#include "./timer.h"
struct timeval start_time[10];
float elapse[10];
//variables are externed to seperate files to ensure proper counts of runntime 
float totalRead = 0;
float totalWrite = 0; 
float totalMetadata = 0; 
float totalWait = 0;
float waitTime = 0;
char* path; 
char* tempLoc;



int my_rank, num_procs;
int numparticles = 8388608;	// 8  meg part icles per process
float *x, *y, *z;
float *px, *py, *pz;
h5part_int32_t *id1, *id2;
int x_dim = 64;
int y_dim = 64; 
int z_dim = 64;

char* file;

// Uniform random number
inline double uniform_random_number() 
{
	return (((double)rand())/((double)(RAND_MAX)));
}

// Initialize particle data
void init_particles ()
{
	int i;
	for (i=0; i<numparticles; i++) 
	{
		id1[i] = i;
		id2[i] = i*2;
		x[i] = uniform_random_number()*x_dim;
		y[i] = uniform_random_number()*y_dim;
		z[i] = ((double)id1[i]/numparticles)*z_dim;    
		px[i] = uniform_random_number()*x_dim;
		py[i] = uniform_random_number()*y_dim;
		pz[i] = ((double)id2[i]/numparticles)*z_dim;
	}
}

// Create BBPart file and write data
void create_and_write_synthetic_h5part_data(int rank) 
{
	
	
	// Note: printf statements are inserted basically 
	// to check the progress. Other than that they can be removed
	BBPartSetStep(file, 0); // only 1 timestep in this file
	
	BBPartWriteFileAttribString(file, "Origin\0", "Tested by Matt\0"); //this should be after step, not before
	
	if (rank == 0) printf ("Step is set \n");
	BBPartSetNumParticles(file, numparticles);
	if (rank == 0) printf ("Num particles is set \n");

	BBPartWriteDataFloat32(file,"x",x); 
	if (rank == 0) printf ("Written variable 1 \n");
	BBPartWriteDataFloat32(file,"y",y);
	if (rank == 0) printf ("Written variable 2 \n");
	BBPartWriteDataFloat32(file,"z",z);
	if (rank == 0) printf ("Written variable 3 \n");

	BBPartWriteDataInt32(file,"id1",id1);
	if (rank == 0) printf ("Written variable 4 \n");
	BBPartWriteDataInt32(file,"id2",id2);
	if (rank == 0) printf ("Written variable 5 \n");
	
	BBPartWriteDataFloat32(file,"px",px); 
	if (rank == 0) printf ("Written variable 6 \n");
	BBPartWriteDataFloat32(file,"py",py);
	if (rank == 0) printf ("Written variable 7 \n");
	BBPartWriteDataFloat32(file,"pz",pz);
	if (rank == 0) printf ("Written variable 8 \n");
}

/*method to check queue write out to final storage location. The large sections of commented code are part of a thread pool for this operation, 
but this is not needed, since every thread will spawn it's own pthread to handle it's queued write requests. */
void *queueEngine(void* input)
{
	int my_rank = (int) input;
	
	//task handler methods for spawned threads - only can accept an int or float write 
	/*
	void *taskHandlerF(void* hName)
	{
		char* h5Name = (char*) hName;
		BBQPartWriteDataFloat32(file, h5Name);
		return NULL;
	}

	void *taskHandlerI(void* hName)
	{
		char* h5Name = (char*) hName;
		BBQPartWriteDataInt32(file, h5Name);
		return NULL;
	}
	*/
	//keeps it from dying to MPI alignment commands, it will throw a segfault if it is executed during an MPI alignment
	sleep(100);
	
	//total copy time
	timer_on(2);
	
	int qs = qSize();
	/*	
	while(qs < 8)
	{
		sleep(3);
		qs = qSize();
	}
	*/
	
	char* req = reqFront();
	char* done = "empty";
	
	int failCount = 0;
	char* lastReq;
	int qPass = 0;
	bool lRun = true;
	
	
	/*
	pthread_t thread1, thread2, thread3, thread4;
	bool t1, t2, t3, t4;
	t1 = false;
	t2 = false; 
	t3 = false;
	t4 = false;
	*/
	 
	while(lRun)
	{
		if(qs > 0) //&& (t1 == false || t2 == false || t3 == false  || t4 == false))
		{		
				/*printf(req);
				*printf(": ");
				*printf(h5NameFront());
				*/
			
			
				if(strcmp(req, "step") == 0)
				{
					//printf(h5NameFront());
					BBQPartSetStep(file);
				}
				else if(strcmp(req, "snp") == 0)
				{
					//printf(h5NameFront());
					BBQPartSetNumParticles(file, h5NameFront());
				}
				else if(strcmp(req, "wdf32") == 0)
				{
					//printf(h5NameFront());
					char* hName = h5NameFront();
					BBQPartWriteDataFloat32(file, hName);
					
					/*
					if(t1 == false)
					{
						t1 = true;
						
						if(pthread_create(&thread1, NULL, taskHandlerF, &hName)) 
						{
							fprintf(stderr, "Error creating thread\n");
							return 1;
						}
					}
					else if(t2 == false)
					{
						t2 = true;
						
						if(pthread_create(&thread2, NULL, taskHandlerF, &hName)) 
						{
							fprintf(stderr, "Error creating thread\n");
							return 1;
						}
					}
					else if(t3 = false)
					{
						t3 = true;
						
						if(pthread_create(&thread3, NULL, taskHandlerF, &hName)) 
						{
							fprintf(stderr, "Error creating thread\n");
							return 1;
						}
					}
					else
					{
						t4 = true;
						
						if(pthread_create(&thread4, NULL, taskHandlerF, &hName)) 
						{
							fprintf(stderr, "Error creating thread\n");
							return 1;
						}
					}
					*/
				}
				else if(strcmp(req, "wdi32") == 0)
				{
					//printf(h5NameFront());
					
					char* hName = h5NameFront();
					
					BBQPartWriteDataInt32(file, hName);

					/*
					if(t1 == false)
					{
						t1 = true;
						
						if(pthread_create(&thread1, NULL, taskHandlerI, &hName)) 
						{
							fprintf(stderr, "Error creating thread\n");
							return 1;
						}
					}
					else if(t2 == false)
					{
						t2 = true;
						
						if(pthread_create(&thread2, NULL, taskHandlerI, &hName)) 
						{
							fprintf(stderr, "Error creating thread\n");
							return 1;
						}
					}
					else if(t3 = false)
					{
						t3 = true;
						
						if(pthread_create(&thread3, NULL, taskHandlerI, &hName)) 
						{
							fprintf(stderr, "Error creating thread\n");
							return 1;
						}
					}
					else
					{
						t4 = true;
						
						if(pthread_create(&thread4, NULL, taskHandlerI, &hName)) 
						{
							fprintf(stderr, "Error creating thread\n");
							return 1;
						}
					}
					*/
					//debug start TODO
					
				}
				else if(strcmp(req, "wfas") == 0)
				{
					//no longer triggers segfault, but constantly errors
					//printf(h5NameFront());
					BBQPartWriteFileAttribString(file, h5NameFront());
				}
				else if(strcmp(req, "open") == 0)
				{
					//printf(h5NameFront());
#ifdef par
					BBQPartOpenFileParallel(file);
#else 
					BBQPartOpenFile(file, my_rank);
#endif
				}
				else if(strcmp(req, "close") == 0)
				{
					//printf(h5NameFront());
					BBQPartCloseFile();
				}
				else
				{
					printf("Not a valid request\n");
				}
		
		
				//sprintf("\n");
		
				Dequeue();
				req = reqFront();
		}
		else if(failCount > 5)
		{
			lRun = false;
		}
		else
		{	
			//because of the wait at the start of this code block this should not be entered. If it is, the wait is 
			//commented, as to not increase the runtime of the application. 
			
			failCount++;
			/*
			if(t1 == true && t2 == true && t3 == true && t4 == true)
			{
				failCount--;
				pthread_join( thread1, NULL);
				pthread_join( thread2, NULL);
				pthread_join( thread3, NULL);
				pthread_join( thread4, NULL);
			}
			*/
			//printf("sleeping");
			//TODO Determine if sleep is needed 
			//sleep(1);
		}
		qs = qSize();
		if(strcmp(req, "empty") == 0)
		{
			qs = 0;
		}
	}
	return NULL;
}



int main (int argc, char* argv[]) 
{
	char *file_name = argv[1];
	
	file = file_name;
	
	//printf("Path: ");
	//printf(argv[1]);
	
	MPI_Init(&argc,&argv);
	MPI_Comm_rank (MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size (MPI_COMM_WORLD, &num_procs);
	//printf("myrank: %d ", my_rank);
	
	path = argv[2];
	
	char* tFile = "/temp.h5";
	strcat(path,tFile);
	tempLoc = path;
	
	if (argc == 4)
	{
		numparticles = (atoi (argv[3]))*1024*1024;
	}
	else
	{
		numparticles = 8*1024*1024;
	}

	if (my_rank == 0) {printf ("Number of particles: %ld \n", numparticles);}

	x=(float*)malloc(numparticles*sizeof(double));
	y=(float*)malloc(numparticles*sizeof(double));
	z=(float*)malloc(numparticles*sizeof(double));

	px=(float*)malloc(numparticles*sizeof(double));
	py=(float*)malloc(numparticles*sizeof(double));
	pz=(float*)malloc(numparticles*sizeof(double));

	id1=(h5part_int32_t*)malloc(numparticles*sizeof(h5part_int32_t));
	id2=(h5part_int32_t*)malloc(numparticles*sizeof(h5part_int32_t));

	init_particles ();
	
	//kills program if the pointers are bad 
	/*
	if(x == NULL || y == NULL || z == NULL || px == NULL || py == NULL || pz == NULL || id1 == NULL || id2 == NULL)
	{
		return 1;
	}
	*/ 
	
	if (my_rank == 0)
	{
		printf ("Finished initializing particles \n");
	}
	
	pthread_t queueEngineT;
	int in = my_rank;	
	if(pthread_create(&queueEngineT, NULL, queueEngine, &in)) 
	{
		fprintf(stderr, "Error creating thread\n");
		return 1;
	}

	MPI_Barrier (MPI_COMM_WORLD);
	timer_on (0);
	//file = BBPartOpenFileParallelAlign(file_name, BBPART_WRITE, MPI_COMM_WORLD, alignf);
	// file = BBPartOpenFileParallel (file_name, BBPART_WRITE | BBPART_VFD_MPIPOSIX | BBPART_FS_LUSTRE, MPI_COMM_WORLD);
	//file = BBPartOpenFileParallel (file_name, BBPART_WRITE | BBPART_FS_LUSTRE, MPI_COMM_WORLD);
	//removed, handled by bb_engine now file = BBPartOpenFileParallel (file_name, BBPART_WRITE, MPI_COMM_WORLD);
	
#ifdef par
	BBPartOpenFileParallel (file);
#else
	BBPartOpenFile (file, my_rank);
#endif

	if (my_rank == 0)
	{
		printf ("Opened BBPart file... \n");
	}
	
	// Throttle and see performance
	// H5PartSetThrottle (file, 10);


	MPI_Barrier (MPI_COMM_WORLD);
	timer_on (1);

	if (my_rank == 0) printf ("Before writing particles \n");
	create_and_write_synthetic_h5part_data(my_rank);


	MPI_Barrier (MPI_COMM_WORLD);
	timer_off (1);
	if (my_rank == 0) printf ("After writing particles \n");

	BBPartCloseFile();
	
	if (my_rank == 0) printf ("After closing particles \n");
	

	free(x);
	free(y);
	free(z);
	
	free(px); 
	free(py); 
	free(pz); 
	
	free(id1); 
	free(id2); 
	
	

	MPI_Barrier (MPI_COMM_WORLD);

	pthread_join(queueEngineT, NULL);

	timer_off (0);
	timer_off(2);
	
	if (my_rank == 0)
	{
		printf ("\nTiming results\n");
		timer_msg (1, "just writing data");
		timer_msg (0, "opening, writing, closing file");
		timer_msg (2, "copying data to final location");
		printf ("\n");
		printf("Total read: ");
		printf("%f\n", totalRead);
		printf("Total write: ");
		printf("%f\n", totalWrite);
		printf("Total metadata operations: ");
		printf("%f\n", totalMetadata);
		printf("Total wait time: ");
		printf("%f\n", totalWait);
	}

	MPI_Finalize();

	return 0;
}
