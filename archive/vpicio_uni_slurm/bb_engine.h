#ifndef queue_h
#define queue_h

void BBQPartCloseFile();
void BBPartCloseFile();
void BBQPartOpenFileParallel(char *path);
void BBPartOpenFileParallel(char *path);
void BBPartOpenFile (char* path, int rank);
void BBQPartOpenFile (char* path, int rank);
void BBQPartWriteFileAttribString(char *path,char *h5Name);
void BBPartWriteFileAttribString(char *path,char *h5Name,char *attrib);
void BBQPartWriteDataInt32(char *path,char *h5Name);
void BBPartWriteDataInt32(char *path,char *h5Name,h5part_int32_t *id);
void BBQPartWriteDataFloat32(char *path,char *h5Name);
void BBPartWriteDataFloat32(char *path,char *h5Name,float *x);
void BBQPartSetNumParticles(char *path,char *h5Name);
void BBPartSetNumParticles(char *path,int nparticles);
void BBQPartSetStep();
void Enqueue(char *p,char *name,char *req);
void BBPartSetStep(char *path,h5part_int64_t step);

#endif
