#PBS -q bench
#PBS -l mppwidth=1
#PBS -l walltime=00:05:00
#PBS -N asim_clean_job
#PBS -j oe 
 
setenv TMP_LOGDATE "`date '+%y%m%d%H%M%S'`"

cd $PBS_O_WORKDIR 
aprun -n 1 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/cleanup_1m.sh >& log_cleanup_1m_$TMP_LOGDATE.log


