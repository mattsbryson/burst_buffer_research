#PBS -q regular
#PBS -l mppwidth=48
#PBS -l walltime=00:30:00
#PBS -N asim_vpicio_job
#PBS -j oe 

setenv CRAY_ROOTFS DSL 
setenv TMP_LOGDATE "`date '+%y%m%d%H%M%S'`"

setenv  PATH /project/projectdirs/m1248/asim_data/bb/edison/h5part-1.6.6/bin:/project/projectdirs/m1248/asim_data/bb/edison/hdf5-1.8.14/bin:$PATH 
setenv LD_LIBRARY_PATH /project/projectdirs/m1248/asim_data/bb/edison/h5part-1.6.6/lib:/project/projectdirs/m1248/asim_data/bb/edison/hdf5-1.8.14/lib:$LD_LIBRARY_PATH 

echo "ls -l /flash/scratch1/asim"
ls -l /flash/scratch1/asim

cd $PBS_O_WORKDIR 
aprun -n 48 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch1/asim/test12.h5 80 >& log_52_1_$TMP_LOGDATE.log 
aprun -n 48 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch2/asim/test22.h5 80 >& log_52_2_$TMP_LOGDATE.log 
aprun -n 48 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch3/asim/test32.h5 80 >& log_52_3_$TMP_LOGDATE.log 
aprun -n 48 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch4/asim/test42.h5 80 >& log_52_4_$TMP_LOGDATE.log 
aprun -n 48 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch5/asim/test52.h5 80 >& log_52_5_$TMP_LOGDATE.log 
aprun -n 48 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch6/asim/test62.h5 80 >& log_52_6_$TMP_LOGDATE.log

