#PBS -q regular
#PBS -l mppwidth=72
#PBS -l walltime=00:30:00
#PBS -N asim_vpicio_job
#PBS -j oe 

setenv CRAY_ROOTFS DSL 
setenv TMP_LOGDATE "`date '+%y%m%d%H%M%S'`"

setenv  PATH /project/projectdirs/m1248/asim_data/bb/edison/h5part-1.6.6/bin:/project/projectdirs/m1248/asim_data/bb/edison/hdf5-1.8.14/bin:$PATH 
setenv LD_LIBRARY_PATH /project/projectdirs/m1248/asim_data/bb/edison/h5part-1.6.6/lib:/project/projectdirs/m1248/asim_data/bb/edison/hdf5-1.8.14/lib:$LD_LIBRARY_PATH 

echo "ls -l /flash/scratch1/asim"
ls -l /flash/scratch1/asim

cd $PBS_O_WORKDIR 
aprun -n 72 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch1/asim/test12.h5 80 >& log_vpicio12_48x48x80_$TMP_LOGDATE.log 

