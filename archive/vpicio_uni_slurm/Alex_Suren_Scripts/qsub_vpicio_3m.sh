#PBS -q bench
#PBS -l mppwidth=48
#PBS -l walltime=01:00:00
#PBS -N asim_vpicio_job
#PBS -j oe 

setenv CRAY_ROOTFS DSL 
setenv TMP_LOGDATE "`date '+%y%m%d%H%M%S'`"

setenv  PATH /project/projectdirs/m1248/asim_data/bb/edison/h5part-1.6.6/bin:/project/projectdirs/m1248/asim_data/bb/edison/hdf5-1.8.14/bin:$PATH 
setenv LD_LIBRARY_PATH /project/projectdirs/m1248/asim_data/bb/edison/h5part-1.6.6/lib:/project/projectdirs/m1248/asim_data/bb/edison/hdf5-1.8.14/lib:$LD_LIBRARY_PATH 

echo "ls -l /flash/scratch3/asim"
ls -l /flash/scratch3/asim

cd $PBS_O_WORKDIR 
aprun -n 48 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch3/asim/test31.h5 80 >& log_vpicio31_48x48x80_$TMP_LOGDATE.log
aprun -n 48 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch3/asim/test32.h5 80 >& log_vpicio32_48x48x80_$TMP_LOGDATE.log
aprun -n 48 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch3/asim/test33.h5 80 >& log_vpicio33_48x48x80_$TMP_LOGDATE.log
aprun -n 48 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch3/asim/test34.h5 80 >& log_vpicio34_48x48x80_$TMP_LOGDATE.log
aprun -n 48 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch3/asim/test35.h5 80 >& log_vpicio35_48x48x80_$TMP_LOGDATE.log
aprun -n 48 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch3/asim/test36.h5 80 >& log_vpicio36_48x48x80_$TMP_LOGDATE.log

