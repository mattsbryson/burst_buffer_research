#PBS -q regular
#PBS -l mppwidth=1
#PBS -l walltime=00:05:00
#PBS -N asim_mkdir_job
#PBS -j oe 
 
setenv TMP_LOGDATE "`date '+%y%m%d%H%M%S'`"

cd $PBS_O_WORKDIR 
aprun -n 1 ./mkdir.sh >& log_mkdir_$TMP_LOGDATE.log


