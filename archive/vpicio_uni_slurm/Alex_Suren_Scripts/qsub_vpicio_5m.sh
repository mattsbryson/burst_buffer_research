#PBS -q regular
#PBS -l mppwidth=48
#PBS -l walltime=01:00:00
#PBS -N asim_vpicio_job
#PBS -j oe 

setenv CRAY_ROOTFS DSL 
setenv TMP_LOGDATE "`date '+%y%m%d%H%M%S'`"

setenv  PATH /project/projectdirs/m1248/asim_data/bb/edison/h5part-1.6.6/bin:/project/projectdirs/m1248/asim_data/bb/edison/hdf5-1.8.14/bin:$PATH 
setenv LD_LIBRARY_PATH /project/projectdirs/m1248/asim_data/bb/edison/h5part-1.6.6/lib:/project/projectdirs/m1248/asim_data/bb/edison/hdf5-1.8.14/lib:$LD_LIBRARY_PATH 

echo "ls -l /flash/scratch5/asim"
ls -l /flash/scratch5/asim

cd $PBS_O_WORKDIR 
aprun -n 48 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch5/asim/test51.h5 80 >& log_vpicio51_48x48x80_$TMP_LOGDATE.log 
aprun -n 48 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch5/asim/test52.h5 80 >& log_vpicio52_48x48x80_$TMP_LOGDATE.log 
aprun -n 48 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch5/asim/test53.h5 80 >& log_vpicio53_48x48x80_$TMP_LOGDATE.log 
aprun -n 48 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch5/asim/test54.h5 80 >& log_vpicio54_48x48x80_$TMP_LOGDATE.log 
aprun -n 48 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch5/asim/test55.h5 80 >& log_vpicio55_48x48x80_$TMP_LOGDATE.log 
aprun -n 48 /project/projectdirs/m1248/asim_data/bb/suren/vpicio_uni/vpicio_uni /flash/scratch5/asim/test56.h5 80 >& log_vpicio56_48x48x80_$TMP_LOGDATE.log

